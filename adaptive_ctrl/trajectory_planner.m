function y = trajectory_planner(a)

global Traiectorie PozitiaDorita perioada_esantionare profil_viteza;

v_d = profil_viteza(Traiectorie.punct,1);
av_d = profil_viteza(Traiectorie.punct,2);
w_d = profil_viteza(Traiectorie.punct,3);
aw_d = profil_viteza(Traiectorie.punct,4);

phi_d = angle_limit(w_d*perioada_esantionare + PozitiaDorita.Phi_dorit);
x_d = v_d*cos(phi_d)*perioada_esantionare + PozitiaDorita.X_dorit;
y_d = v_d*sin(phi_d)*perioada_esantionare + PozitiaDorita.Y_dorit;

PozitiaDorita.Phi_dorit = phi_d;
PozitiaDorita.X_dorit = x_d;
PozitiaDorita.Y_dorit = y_d;
Traiectorie.punct = Traiectorie.punct + 1;

y = [v_d; av_d; w_d; aw_d; x_d; y_d; phi_d];