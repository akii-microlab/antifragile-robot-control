function sig = semn(b)
    if (b < 0)
        sig = -1;
    else
        sig = 1;
    end;
