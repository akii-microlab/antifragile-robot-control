function sig = signum(b)
    if (b < 0)
        sig = -1;
    elseif (b > 0)
        sig = 1;
    elseif (b==0)
        sig = 0.001;
    end;
