function W=inverse_kinematics(viteza)

global DefParametri;

Vc=viteza(1,1);
Wc=viteza(2,1);

W=1/DefParametri.raza_roata*[1 DefParametri.raza_robot;1 -DefParametri.raza_robot]*[Vc;Wc];
