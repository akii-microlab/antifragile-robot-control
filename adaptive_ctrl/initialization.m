load ALL_ENV_VARS_SET.mat
%function init
global Traiectorie DefParametri ParametriCtrl Odometrie PozitiaDorita ...
    perioada_esantionare profil_viteza timp_final v_c w_c

profil_viteza = load('traiectorie_demo_complexa.txt');

perioada_esantionare=0.2;

timp_final = length(profil_viteza)*perioada_esantionare-...
    perioada_esantionare;

%timp_final = 150;
v_c = 0.00;
w_c = 0.00;

Traiectorie = struct ('punct', 1,...
    'eroare_max',    0.1);

DefParametri = struct('raza_robot', 0.3,...
    'raza_roata',0.08,...
    'nr_revolutii', 500);

ParametriCtrl = struct('h', 0.01,...
    'timp_simulare',    100,...
    'Kp',      0.84,...
    'Ki',      0.5);

Odometrie = struct('poz_X', 0,...
    'poz_Y', 0,...
    'Teta', 0);

PozitiaDorita = struct('X_dorit', 0,...
    'Y_dorit', 0,...
    'Phi_dorit', 0);

% Vin is the input voltage to the motor
% i is the motor current
% th is the rotor angle, theta
% dth is the rotor angular velocity sometimes called omega

Ts = 0.1;% sampling time
Tstop = 400*Ts;% simulation period
Rm  = 1;             % Motor resistance (ohm)
Lm  = 0.01;         % motor inductance (Henrys)
Kb = 0.01; % Back EMF constant (Volt-sec/Rad)
Kt = 0.01;            % Torque constand (Nm/A)
Jm = 1.0e-5;           % Rotor inertia (Kg m^2)
bm = 12e-7;      % MEchanical damping (linear model of friction: bm * dth)

% SYSTEM MATRICES
%
% States:  [i dth th]'
% Input:   Vin the motor voltage
% Outputs: same as states
%
% Note that the last state, dth, can be removed in many cases
% when the actual angle is not needed.
%
A = [-Rm/Lm   -Kb/Lm   0;
    Kt/Jm   -bm/Jm   0;
    0        1     0];

B = [1/Lm 0 0]';

C = [1 0 0; 0 1 0; 0 0 1];

D = [0 0 0]';

sys = ss(A, B, C, D);

x0 = zeros(3,1);

num_right = K_r/Tp_r;
den_right = [1 1/Tp_r];

num_left = K_l/Tp_l;
den_left = [1 1/Tp_l];

[A1,B1,C1,D1] = tf2ss(num_left,den_left);
sys1 = ss(A1,B1,C1,D1);

[A2,B2,C2,D2] = tf2ss(num_right,den_right);
sys2 = ss(A2,B2,C2,D2);

Kp_1 = 0.91222;
Ti_1 = 0.77088;
Ki_1 = Kp_1/Ti_1;

Kp_2 = 0.96073;
Ti_2 = 0.75434;
Ki_2 = Kp_2/Ti_2;

save ALL_ENV_VARS_SET.mat