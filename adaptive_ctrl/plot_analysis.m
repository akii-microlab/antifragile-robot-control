clear; clc;

load val_xyphi.mat
load val_viteza.mat
load sliding_val

figure(1); set(gcf, 'color','w');
subplot(2,1,1)
plot(xyphi(1,:),xyphi(8,:),'b', xyphi(1,:), xyphi(9,:),'--r','LineWidth',1.5)
legend('x_e','y_e')
xlabel('time [s]','fontsize',12)
ylabel('Errors x_e and y_e [m]','fontsize',12)
set(gca,'fontsize',12);
subplot(2,1,2)
plot(xyphi(1,:),xyphi(10,:).*180/pi,'b','LineWidth',1.5)
legend('\phi_e')
xlabel('time [s]','fontsize',12)
ylabel('Angular errror \phi_e [deg]','fontsize',12)
set(gca,'fontsize',12);

acc_c = zeros(1, length(viteza) - 1);
acc_r = zeros(1, length(viteza)- 1);
acc_wc = zeros(1, length(viteza)- 1);
acc_wr = zeros(1, length(viteza)- 1);

k=1;
for i = 0:0.05:length(viteza(1,:))*0.05-0.1
    acc_c(k)= (viteza(8,k+1) - viteza(8,k))/0.05;
    acc_r(k)= (viteza(6,k+1) - viteza(6,k))/0.05;

    acc_wc(k)=viteza(8,k).*(viteza(9,k));
    acc_wr(k)=viteza(6,k).*(viteza(7,k));

    k=k+1;
end

time=viteza(1,:);
time_a = 0:0.05:length(viteza(1,:))*0.05-0.1;

figure(3); set(gcf, 'color', 'w'); box off;
plot3(ones(1,length(time)),time,viteza(3,:),'b','LineWidth',1.5)
hold on
plot3(1.5*ones(1,length(time_a)),4*time_a,acc_c,'g','LineWidth',1.5)
plot3(2*ones(1,length(time_a)),4*time_a,acc_r,'r','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(3,:),'b','LineWidth',1.5)
plot3(2.5*ones(1,length(time_a)),4*time_a,acc_c,'g','LineWidth',1.5)
plot3(2.5*ones(1,length(time_a)),4*time_a,acc_r,'r','LineWidth',1.5)
ylabel('time [s]','fontsize',12)
zlabel('Linear accelerations [m/s^2]','fontsize',12)
% axis([1 2.5 0 51 -0.75 0.75])
axis([1 2.5 0 65 -0.75 0.75])
set(gca,'fontsize',12);
set(gca,'YTick',[0 10 20 30 40 50],'XTickLabel',{'av_d'; 'av_c'; 'av_r'; 'av_d, av_c, av_r'})
set(gca,'YDir','reverse')
hold off
grid


figure(4); set(gcf, 'color', 'w'); box off;
plot3(ones(1,length(time)),time,viteza(5,:),'b','LineWidth',1.5)
hold on
plot3(1.5*ones(1,length(time_a)),4*time_a,acc_wc,'g','LineWidth',1.5)
plot3(2*ones(1,length(time_a)),4*time_a,acc_wr,'r','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(5,:),'b','LineWidth',1.5)
plot3(2.5*ones(1,length(time_a)),4*time_a,acc_wc,'g','LineWidth',1.5)
plot3(2.5*ones(1,length(time_a)),4*time_a,acc_wr,'r','LineWidth',1.5)
ylabel('time [s]','fontsize',12)
zlabel('Angular accelerations [rad/s^2]','fontsize',12)
%axis([1 2.5 0 51 -0.4 0.4])
axis([1 2.5 0 65 -0.4 0.4])
set(gca,'fontsize',12);
set(gca,'YTick',[0 10 20 30 40 50],'XTickLabel',{'aw_d'; 'aw_c'; 'aw_r'; 'aw_d, aw_c, aw_r'})
set(gca,'YDir','reverse')
hold off
grid


figure(5); set(gcf, 'color','w');
plot3(ones(1,length(time)),time,viteza(2,:),'b','LineWidth',1.5)
hold on
plot3(1.5*ones(1,length(time)),time,viteza(8,:),'g','LineWidth',1.5)
plot3(2*ones(1,length(time)),time,viteza(6,:),'r','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(2,:),'b','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(8,:),'g','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(6,:),'r','LineWidth',1.5)
ylabel('time [s]','fontsize',12)
zlabel('Linear vitezacity [m/s]','fontsize',12)
% axis([1 2.5 0 51 0 1])
axis([1 2.5 0 65 0 1])
set(gca,'fontsize',12);
set(gca,'YTick',[0 10 20 30 40 50],'XTickLabel',{'v_d'; 'v_c'; 'v_r'; 'v_d, v_c, v_r'})
set(gca,'YDir','reverse')
hold off
grid

figure(6); set(gcf, 'color','w');
plot3(ones(1,length(time)),time,viteza(4,:),'b','LineWidth',1.5)
hold on
plot3(1.5*ones(1,length(time)),time,viteza(9,:),'g','LineWidth',1.5)
plot3(2*ones(1,length(time)),time,viteza(7,:),'r','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(4,:),'b','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(9,:),'g','LineWidth',1.5)
plot3(2.5*ones(1,length(time)),time,viteza(7,:),'r','LineWidth',1.5)
ylabel('time [s]','fontsize',12)
zlabel('Angular vitezacity [rad/s]','fontsize',12)
axis([1 2.5 0 65 -0.6 0.6])
set(gca,'fontsize',12);
set(gca,'YTick',[0 10 20 30 40 50],'XTickLabel',{'w_d'; 'w_c'; 'w_r'; 'w_d, w_c, w_r'})
set(gca,'YDir','reverse')
hold off
grid

load("traiectorie_robot.mat"); load("traiectorie_ideala.mat");
figure;set(gcf, "Color", "w"); axis off;
traiectorie_ideala_adaptive = traiectorie_ideala;
traiectorie_robot_adaptive = traiectorie_robot;
save("traiectorie_robot_adaptive_ctrl.mat",...
    'traiectorie_ideala_adaptive', 'traiectorie_robot_adaptive');
plot(traiectorie_ideala(2,:), traiectorie_ideala(3, :)); hold on;
plot(traiectorie_robot(2,:), traiectorie_robot(3, :));
box off;

