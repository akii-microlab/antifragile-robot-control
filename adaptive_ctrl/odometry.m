function out=odometry(in)
global DefParametri Odometrie vv ww perioada_esantionare;

Nd=in(1,1);
Ne=in(2,1);

Kr=pi*DefParametri.raza_roata/DefParametri.nr_revolutii;
phi=Kr/DefParametri.raza_robot*(Nd-Ne);
DeltaS=Kr*(Nd+Ne);

delta_x = DeltaS*cos(Odometrie.Teta + phi/2);
delta_y = DeltaS*sin(Odometrie.Teta + phi/2);

vv(end+1)=sqrt(delta_x^2 + delta_y^2)/perioada_esantionare;
ww(end+1)=phi/perioada_esantionare;

X = Odometrie.poz_X + delta_x;
Y = Odometrie.poz_Y + delta_y;
Teta = angle_limit(Odometrie.Teta + phi);


Odometrie.poz_X = X;
Odometrie.poz_Y = Y;
Odometrie.Teta = Teta;

out=[vv(end); ww(end); X; Y; Teta];