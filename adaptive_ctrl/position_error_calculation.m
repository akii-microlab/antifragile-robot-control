function errors_pos_ex = position_error_calculation(input)

x_d = input(1);
y_d = input(2);
phi_d = input(3);

x_r = input(4);
y_r = input(5);
phi_r = input(6);

e_x = (x_r-x_d)*cos(phi_d)+(y_r-y_d)*sin(phi_d);
e_y = -(x_r-x_d)*sin(phi_d)+(y_r-y_d)*cos(phi_d);
e_phi = angle_limit(phi_r - phi_d);

errors_pos_ex = [e_x; e_y; e_phi];