function y = antifragile_ctrl(vector_intrare)

global perioada_esantionare v_c w_c

k1=0.75;
k2=20;
k0 = k2/(pi/4);

Q1=0.05;
Q2=0.05;
P1=0.75;
P2=1.75;

% collecting error components
v_d =  vector_intrare(1);
a_v =  vector_intrare(2);
w_d =  vector_intrare(3);
a_w =  vector_intrare(4);

e_x = vector_intrare(5);
e_y = vector_intrare(6);
e_phi = vector_intrare(7);

v_r = vector_intrare(8);
w_r = vector_intrare(9);

% Derivata erorii
e_x_der = -v_d + v_r*cos(e_phi) + e_y*w_d;
e_y_der = v_r*sin(e_phi) - e_x*w_d;
e_phi_der = w_r-w_d;

if  e_y < 0
    k2 = - abs(k2);
elseif e_y > 0
    k2 = abs(k2);
end

% sliding surfaces

s1 = e_x_der + k1*e_x;
s2 = e_y_der + k2*e_y + k0*signum(e_y)*e_phi;

temp_1 = (-P1*s1 - Q1*satlin(s1/0.15) - k1*e_x_der - a_w*e_y - w_d*e_y_der + a_v);
temp_2 = (-P2*s2 - Q2*satlin(s2/0.15) - k2*e_y_der + a_w*e_x + w_d*e_x_der);

v_c_der = (temp_1 + v_r*e_phi_der*sin(e_phi))/cos(e_phi);
v_c_next = perioada_esantionare*v_c_der + v_c;

w_c = (temp_2 - v_c_der*sin(e_phi))/(v_r*cos(e_phi)+k0*signum(e_y))+w_d;
v_com = v_c;
v_c = v_c_next;

y = [v_com; w_c; e_x_der; e_y_der; e_phi_der; s1; s2];
end