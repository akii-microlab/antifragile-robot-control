function out = rms(in,dim)

if nargin == 1      % Default (no dim argument specified)
    dim = 1;
end

if nargin==1 && ismatrix(in) && any(size(in)==1)       
    out = norm(in)/sqrt(length(in));        
else
    out = sqrt(sum((in.^2),dim)/size((in),dim));       
end
