function W=inverse_kinematics(viteza)

global DefParametri;

Vc=viteza(1,1);
Wc=viteza(2,1);

raza_r_stanga = DefParametri.raza_roata_stanga;
raza_r_dreapta = DefParametri.raza_roata_dreapta;

W=1/((raza_r_stanga + raza_r_dreapta)/2)*[1 DefParametri.raza_robot;1 -DefParametri.raza_robot]*[Vc;Wc];
