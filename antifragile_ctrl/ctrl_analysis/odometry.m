function out=odometry(in)

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

global DefParametri Odometrie vv ww perioada_esantionare;

Nd=in(1,1);
Ne=in(2,1);
fault_type_flag = in(3,1);
time = in(4,1);

raza_r_stanga = DefParametri.raza_roata_stanga;
raza_r_dreapta = DefParametri.raza_roata_dreapta;

switch (fault_type_flag)

    case 0
        raza_r_stanga = DefParametri.raza_roata_stanga;
        raza_r_dreapta = DefParametri.raza_roata_dreapta;
    case 1

        raza_r_dreapta = 0.05;
        raza_r_stanga = DefParametri.raza_roata_stanga;

    case 2

        raza_r_stanga = 0.04;
        raza_r_dreapta = DefParametri.raza_roata_dreapta;

    case 3
        if (mod((time),0.20)==0)

            raza_r_dreapta = 0.28;
            raza_r_stanga = DefParametri.raza_roata_stanga;
        end

    case 4
        if (mod(time,0.20)==0)

            raza_r_stanga = 0.27;
            raza_r_dreapta = DefParametri.raza_roata_dreapta;

        end
    otherwise
        raza_r_stanga = DefParametri.raza_roata_stanga;
        raza_r_dreapta = DefParametri.raza_roata_dreapta;
end

Kr=pi*((raza_r_stanga + raza_r_dreapta)/2)/DefParametri.nr_revolutii;
phi=Kr/DefParametri.raza_robot*(Nd-Ne);
DeltaS=Kr*(Nd+Ne);

delta_x = DeltaS*cos(Odometrie.Teta + phi/2);
delta_y = DeltaS*sin(Odometrie.Teta + phi/2);

vv(end+1)=sqrt(delta_x^2 + delta_y^2)/perioada_esantionare;
ww(end+1)=phi/perioada_esantionare;

X = Odometrie.poz_X + delta_x;
Y = Odometrie.poz_Y + delta_y;
Teta = angle_limit(Odometrie.Teta + phi);

Odometrie.poz_X = X;
Odometrie.poz_Y = Y;
Odometrie.Teta = Teta;

out=[vv(end); ww(end); X; Y; Teta];