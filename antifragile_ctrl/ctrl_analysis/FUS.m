function reconf_ctrl = FUS(in)

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

% Kalman Filter based fault modelling and prediction

global fault_flag fault_type  ...
    % rezidual1 rezidual2 rezidual3 rezidual4 ...
    % eta_buffer1 eta_buffer2 eta_buffer3 eta_buffer4;

fault_type = 0;
fault_flag = 0;
Xk_prev0 = [0;0;0];

Xk_prev1 = [0;0;0];

Xk_prev2 = [0;0;0];

Xk_prev3 = [0;0;0];

Xk_prev4 = [0;0;0];

sigma_model0 = 0.01;
P0 = sigma_model0^2*eye(3);

sigma_model1 = 0.01;
P1 = sigma_model1^2*eye(3);

sigma_model2 = 0.01;
P2 = sigma_model2^2*eye(3);

sigma_model3 = 0.01;
P3 = sigma_model3^2*eye(3);

sigma_model4 = 0.01;
P4 = sigma_model4^2*eye(3);

sum_eta0 = [0;0;0];
sum_eta1 = [0;0;0];
sum_eta2 = [0;0;0];
sum_eta3 = [0;0;0];
sum_eta4 = [0;0;0];

N = 323; % samples
k = 1;

% loop

for j = 0:0.2:64.4
    
    sigma_meas = 0.01;
    % Measurements vector
    % actual pose of the robot
    
    Z = [in(1);in(2);in(3)]+sigma_meas*rand;

    % Fault free EKF filter
    
    % robot wheel radii
    rR0 = 0.08;
    rL0 = 0.08;
    
    % Q system's noise covariance matrix
    % deviations for all three dimensions
    
    sigma_x0 = 0.1;
    sigma_y0 = 0.1;
    sigma_teta0= 0.1;
    
    Q0 = [sigma_x0^2,   0,   0;
        0,    sigma_y0^2,  0;
        0,    0,   sigma_teta0^2];
    
    % R covariance matrix of measurement noise
    
    sigma_meas0 = 0.01;
    R0 = sigma_meas0^2*eye(3);
      
    % EKF PREDICTION STEP
    
    Phi0 = [1,0, -1/500*pi*(1/2*rR0+1/2*rL0)*(in(4)+in(5))*sin(in(3)+1/300*pi*(1/2*rR0+1/2*rL0)*(in(4)-in(5)));
        0,1,  1/500*pi*(1/2*rR0+1/2*rL0)*(in(4)+in(5))*cos(in(3)+1/300*pi*(1/2*rR0+1/2*rL0)*(in(4)-in(5)));
        0,0,                                                                                            1];
    
    P10 = Phi0*P0*Phi0' + Q0;
    
    H0 = eye(3);
    
    % EKF CORRECTION STEP
    
    S0 = H0*P10*H0' + R0;
    
    % K is the EKF gain.
    K0 = P10*H0'*inv(S0);
    
    % update covariance matrix
    % P = P1 - K*H*P1;
    % using the more efficient Joseph form
    P0 = (eye(3)-K0*H0)*P10*(eye(3)-K0*H0)'+K0*R0*K0';
    
    % residual calculation
    r0 = Z - H0*Phi0*Xk_prev0;
    
    % state estimation
    Xk0 = Phi0*Xk_prev0 + K0*r0;
    
    % inovation computation
    
    etaS0 = inv(sqrt(S0))*r0;
    
    sum_eta0 = sum_eta0 + etaS0;
    
    etaS_hat0  = 1/N*sum_eta0;
    
    etaS_hat0 = real(etaS_hat0);
    
    %% EKF Right wheel flat tyre

    rR1 = 0.05;
    rL1 = 0.08;

    sigma_x1 = 0.1;
    sigma_y1 = 0.1;
    sigma_teta1= 0.1;
    
    Q1 = [sigma_x1^2,   0,   0;
        0,    sigma_y1^2,  0;
        0,    0,   sigma_teta1^2];
    
    sigma_meas1 = 0.01;
    
    R1 = sigma_meas1^2*eye(3);

    Phi1 = [1,0, -1/500*pi*(1/2*rR1+1/2*rL1)*(in(4)+in(5))*sin(in(3)+1/300*pi*(1/2*rR1+1/2*rL1)*(in(4)-in(5)));
        0,1,  1/500*pi*(1/2*rR1+1/2*rL1)*(in(4)+in(5))*cos(in(3)+1/300*pi*(1/2*rR1+1/2*rL1)*(in(4)-in(5)));
        0,0,                                                                                            1];
    
    
    P11 = Phi1*P1*Phi1' + Q1;
    
    H1 = eye(3);
   
    S1 = H1*P11*H1' + R1;

    K1 = P11*H1'*inv(S1);
    
    P1 = (eye(3)-K1*H1)*P11*(eye(3)-K1*H1)'+K1*R1*K1';
    
    r1 = Z - H1*Phi1*Xk_prev1;
    
    Xk1 = Phi1*Xk_prev1 + K1*r1;
    
    etaS1 = inv(sqrt(S1))*r1;
    
    sum_eta1 = sum_eta1 + etaS1;
    
    etaS_hat1  = 1/N*sum_eta1;
    
    etaS_hat1 = real(etaS_hat1);
    
    %% EKF Left wheel flat tyre

    rR2 = 0.08;
    rL2 = 0.04;

    sigma_x2 = 0.1;
    sigma_y2 = 0.1;
    sigma_teta2= 0.1;
    
    Q2 = [sigma_x2^2,   0,   0;
        0,    sigma_y2^2,  0;
        0,    0,   sigma_teta2^2];
    
    sigma_meas2 = 0.01;
    
    R2 = sigma_meas2^2*eye(3);
    
    Phi2= [1,0, -1/500*pi*(1/2*rR2+1/2*rL2)*(in(4)+in(5))*sin(in(3)+1/300*pi*(1/2*rR2+1/2*rL2)*(in(4)-in(5)));
        0,1,  1/500*pi*(1/2*rR2+1/2*rL2)*(in(4)+in(5))*cos(in(3)+1/300*pi*(1/2*rR2+1/2*rL2)*(in(4)-in(5)));
        0,0,                                                                                            1];
    
    
    P12 = Phi2*P2*Phi2' + Q2;
    
    H2 = eye(3);
    
    S2 = H2*P12*H2' + R2;

    K2 = P12*H2'*inv(S2);

    P2 = (eye(3)-K2*H2)*P12*(eye(3)-K2*H2)'+K2*R2*K2';
    
    r2 = Z - H2*Phi2*Xk_prev2;

    Xk2 = Phi2*Xk_prev2 + K2*r2;
      
    etaS2 = inv(sqrt(S2))*r2;
    
    sum_eta2 = sum_eta2+ etaS2;
    
    etaS_hat2  = 1/N*sum_eta2;
    
    etaS_hat2 = real(etaS_hat2);
    
    %% EKF Right wheel shaft bump

    rR3 = 0.08;
    rL3 = 0.08;

    sigma_x3= 0.1;
    sigma_y3 = 0.1;
    sigma_teta3= 0.1;
    
    Q3 = [sigma_x3^2,   0,   0;
        0,    sigma_y3^2,  0;
        0,    0,   sigma_teta3^2];

    sigma_meas3 = 0.01;
    
    R3 = sigma_meas3^2*eye(3);
    
    if (mod((in(6)),0.2)==0)
        
        rR3 =0.28;
    end
    
    Phi3= [1,0, -1/500*pi*(1/2*rR3+1/2*rL3)*(in(4)+in(5))*sin(in(3)+1/300*pi*(1/2*rR3+1/2*rL3)*(in(4)-in(5)));
        0,1,  1/500*pi*(1/2*rR3+1/2*rL3)*(in(4)+in(5))*cos(in(3)+1/300*pi*(1/2*rR3+1/2*rL3)*(in(4)-in(5)));
        0,0,                                                                                            1];
    
    
    P13 = Phi3*P3*Phi3' + Q3;
    
    H3= eye(3);
    
    S3 = H3*P13*H3' + R3;
    
    K3 = P13*H3'*inv(S3);

    P3 = (eye(3)-K3*H3)*P13*(eye(3)-K3*H3)'+K3*R3*K3';
    
    r3 = Z - H3*Phi3*Xk_prev3;
    
    Xk3 = Phi3*Xk_prev3 + K3*r3;
        
    etaS3 = inv(sqrt(S3))*r3;
    
    sum_eta3 = sum_eta3 + etaS3;
    
    etaS_hat3  = 1/N*sum_eta3;
    
    etaS_hat3 = real(etaS_hat3);
    
    %% EKF Left wheel shaft bump

    rR4 = 0.08;
    rL4 = 0.08;

    sigma_x4= 0.1;
    sigma_y4 = 0.1;
    sigma_teta4= 0.1;
    
    Q4 = [sigma_x4^2,   0,   0;
        0,    sigma_y4^2,  0;
        0,    0,   sigma_teta4^2];
    
    sigma_meas4 = 0.01;
    
    R4 = sigma_meas4^2*eye(3);
    
    if (mod(in(6),0.2)==0)
        
        rL4 = 0.27;
        
    end
    
    Phi4= [1,0, -1/500*pi*(1/2*rR4+1/2*rL4)*(in(4)+in(5))*sin(in(3)+1/300*pi*(1/2*rR4+1/2*rL4)*(in(4)-in(5)));
        0,1,  1/500*pi*(1/2*rR4+1/2*rL4)*(in(4)+in(5))*cos(in(3)+1/300*pi*(1/2*rR4+1/2*rL4)*(in(4)-in(5)));
        0,0,                                                                                            1];
    
    
    P14 = Phi4*P4*Phi4' + Q4;
    
    H4 = eye(3);
    
    S4 = H4*P14*H4' + R4;
    
    K4 = P14*H4'*inv(S4);
    
    P4 = (eye(3)-K4*H4)*P14*(eye(3)-K4*H4)'+K4*R4*K4';
    
    r4 = Z - H4*Phi4*Xk_prev4;
    
    Xk4 = Phi4*Xk_prev4 + K4*r4;
    
    etaS4 = inv(sqrt(S4))*r4;
    
    sum_eta4 = sum_eta4 + etaS4;
    
    etaS_hat4  = 1/N*sum_eta4;
    
    etaS_hat4 = real(etaS_hat4);
    
    etaS_hat_global = [etaS_hat1,etaS_hat2,etaS_hat3,etaS_hat4];
    
    Xk_prev0 = Xk0;
    
    Xk_prev1 = Xk1;
    
    Xk_prev2 = Xk2;
    
    Xk_prev3 = Xk3;
    
    Xk_prev4 = Xk4;
 end 
reconf_ctrl = 0;
end