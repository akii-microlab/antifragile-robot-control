% extended plots

load traiectorie_ideala.mat
load traiectorie_robot.mat
load V_R.mat
load V_C.mat
load W_R.mat
load W_C.mat
load Xe.mat
load Ye.mat
load Tetae.mat
load WREF_S.mat
load WMAS_S.mat
load WREF_D.mat
load WMAS_D.mat

figure(1); set(gcf, 'color', 'w'); box off;
plot(traiectorie_ideala(2,:),traiectorie_ideala(3,:),'+r',traiectorie_robot(2,:),traiectorie_robot(3,:),'-b');
title('Comparison between the virtual robot trajectory and the real robot behaviour');
xlabel('X [m] ');
ylabel('Y [m] ');
grid; box off;

figure(2); set(gcf, 'color', 'w'); box off;
plot(V_R(1,:),V_R(2,:),'-r',V_C(1,:),V_C(2,:),'-b');
title('Linear velocity (m/s)');
legend('Vr','Vc');
xlabel('time [s]');
grid; box off;

figure(3); set(gcf, 'color', 'w'); box off;
plot(W_R(1,:),W_R(2,:),'-r',W_C(1,:),W_C(2,:),'-b');
title('Angular velocity (m/s)');
legend('Wr','Wc');
xlabel('time [s]');
grid; box off;

figure(4); set(gcf, 'color', 'w'); box off;
plot(Xe(1,:),Xe(2,:),'-b');
title('Longitudinal error, Xe[m] ');
xlabel('time [s]');
grid; box off;

figure(5); set(gcf, 'color', 'w'); box off;
plot(Ye(1,:),Ye(2,:),'-b');
title('Lateral error, Ye[m] ');
xlabel('time [s]');
grid; box off;

figure(6); set(gcf, 'color', 'w'); box off;
plot(Tetae(1,:),Tetae(2,:),'-b');
title('Heading error, Tetae[rad] ');
xlabel('time [s]');
grid; box off;

figure(7); set(gcf, 'color', 'w'); box off;
plot(WREF_S(1,:),WREF_S(2,:),'-r',WMAS_S(1,:),WMAS_S(2,:),'-b');
title('Motor 2 response');
legend('Wref','Wmas');
xlabel('time [s]');
grid; box off;

figure(8); set(gcf, 'color', 'w'); box off;
plot(WREF_D(1,:),WREF_D(2,:),'-r',WMAS_D(1,:),WMAS_D(2,:),'-b');
title('Motor 1 response');
legend('Wref','Wmas');
xlabel('time [s]');
grid; box off;


