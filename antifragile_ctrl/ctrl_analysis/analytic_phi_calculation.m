
% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

syms Xtrue Ytrue Tetatrue Ts vRobot wRobot wR wL rR rL fx fy fteta d L ...
    dt lsamp rsamp Kr phi DeltaS delta_x delta_y

state = [Xtrue;Ytrue;Tetatrue];

Kr=pi*((rR+rL)/2)/N;
phi=Kr/Rrob*(rsamp - lsamp);
DeltaS=Kr*(rsamp + lsamp);

delta_x = DeltaS*cos(Tetatrue + phi/2);
delta_y = DeltaS*sin(Tetatrue + phi/2);

fx = Xtrue + delta_x;
fy = Ytrue + delta_y;
fteta = Tetatrue + phi;

f = [fx;fy;fteta];

Phi = jacobian(f,state);

Z = [Xtrue;Ytrue;Tetatrue] + sigma_meas*randn;

H = jacobian(Z,state);
