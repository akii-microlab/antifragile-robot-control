function errors_pos_ex = position_error_calculation(input)

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

x_d = input(1);
y_d = input(2);
phi_d = input(3);

x_r = input(4);
y_r = input(5);
phi_r = input(6);

e_x = (x_r-x_d)*cos(phi_d)+(y_r-y_d)*sin(phi_d);
e_y = -(x_r-x_d)*sin(phi_d)+(y_r-y_d)*cos(phi_d);
e_phi = angle_limit(phi_r - phi_d);

errors_pos_ex = [e_x; e_y; e_phi];