% parametrization of PID controller for the robot actuators
Ts=perioada_esantionare;

K_r = 0.96073;
Tp_r = 0.75434;

K_l = 0.91222;
Tp_l = 0.77088;

Ke = 0.023;
Ra = 0.71;
tr = 19.7;

num_right = K_r/Tp_r;
den_right = [1 1/Tp_r];

num_left = K_l/Tp_l;
den_left = [1 1/Tp_l];

Gmot_right=tf(num_right,den_right);
Gmot_left=tf(num_left,den_left);

[numz_right,denz_right]=c2dm(num_right,den_right,Ts,'zoh');
[numz_left,denz_left]=c2dm(num_left,den_left,Ts,'zoh');

zeta = 1;
wn = 15;

wd = wn*sqrt(1-zeta^2);

a1=-2*exp(-zeta*wn*Ts)*cos(wd*Ts);
a2=exp(-2*zeta*wn*Ts);

a_left = numz_left(2);
b_left = -denz_left(2);

Ki_left = ((a1+a2+1)/(a_left*Ts));
Kp_left = ((a1+b_left+1)/(0.5*a_left)-Ki_left*Ts)/2;

a_right = numz_right(2);
b_right = -denz_right(2);

Ki_right = ((a1+a2+1)/(a_right*Ts));
Kp_right = ((a1+b_right+1)/(0.5*a_right)-Ki_right*Ts)/2;

numz_c_left = [(2*Kp_left+Ki_left*Ts) (Ki_left*Ts-2*Kp_left)];
denz_c_left = [2 -2];

numz_c_right = [(2*Kp_right+Ki_right*Ts) (Ki_right*Ts-2*Kp_right)];
denz_c_right = [2 -2];