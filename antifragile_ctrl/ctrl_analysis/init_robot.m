clear; clc;

% based on the work in Axenie, Cristian, and Daniela Cernega.
% "Adaptive sliding mode controller design for mobile robot
% fault tolerant control: Itroducing ARTEMIC."
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region
% (RAAD 2010). IEEE, 2010.
%
% and
%
% Axenie, Cristian, and Razvan Solea.
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems
% and Applications. IEEE, 2010.

load eta_buffer0.txt

%function init
global Traiectorie DefParametri ParametriCtrl Odometrie Odometrie2 ...
    PozitiaDorita perioada_esantionare profil_viteza timp_final v_c w_c

profil_viteza = load('traiectorie_demo_complexa.txt');

perioada_esantionare=0.2;

timp_final = length(profil_viteza)*perioada_esantionare-...
    perioada_esantionare;

%timp_final = 150;
v_c = 0.00;
w_c = 0.00;

Traiectorie = struct ('punct', 1,... 
    'eroare_max',    0.1);   

DefParametri = struct('raza_robot', 0.3,...
    'raza_roata_stanga',0.08,...
    'raza_roata_dreapta', 0.08,...
    'nr_revolutii', 500);

ParametriCtrl = struct('h', 0.01,...
    'timp_simulare',    100,...          
    'Kp',      0.84,...       
    'Ki',      0.5);           

Odometrie = struct('poz_X', 0,...
    'poz_Y', 0,...
    'Teta', 0);

Odometrie2 = struct('poz_X', 0,...
    'poz_Y', 0,...
    'Teta', 0);

PozitiaDorita = struct('X_dorit', 0,...
    'Y_dorit', 0,...
    'Phi_dorit', 0);

% synthesize the PID controller of the wheels
singular_perturbation_pid_synthesis

