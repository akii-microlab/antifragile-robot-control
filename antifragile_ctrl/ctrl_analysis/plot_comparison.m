% fault free vs faulty operation performance
load eroare_robot_fault_bump_shaft_left
eroare_robot_fault  = eroare_robot_fault;
load eroare_robot_fault
eroare_fault_free = eroare_robot_fault;

figure(1); set(gcf, 'color', 'w'); box off;
plot(eroare_fault_free(1,:),eroare_fault_free(2,:),'-r');
hold on
plot(eroare_robot_fault(1,:),eroare_robot_fault(2,:),'-b');
title('Error on X (desired-measured) in the case without defects and with shaft bump left');
figure(2); set(gcf, 'color', 'w'); box off;
plot(eroare_fault_free(1,:),eroare_fault_free(3,:),'-r');
hold on;  box off;
plot(eroare_robot_fault(1,:),eroare_robot_fault(3,:),'-b');
title('Error on Y (desired-measured) in the case without defects and with shaft bump left');
figure(3); set(gcf, 'color', 'w'); box off;
plot(eroare_fault_free(1,:),eroare_fault_free(4,:),'-r');
hold on;  box off;
plot(eroare_robot_fault(1,:),eroare_robot_fault(4,:),'-b');
title('Error on \theta (desired-measured) in the case without defects and with shaft bump left');
box off;