% simulate the control
fuzzy_ctrl; 
% figure for quantitative analysis
traiectorie_ideala_fuzzy = [xr, yr];
traiectorie_robot_fuzzy = [x, y];
traiectorie_robot_fuzzy = traiectorie_robot_fuzzy(1:length(traiectorie_ideala_fuzzy), :);

t = linspace(1, 140, 140);
ti = linspace(1, 323,  323);

traiectorie_robot_fuzzy = interp1(t, traiectorie_robot_fuzzy, ti, 'linear','extrap');
traiectorie_ideala_fuzzy = interp1(t, traiectorie_ideala_fuzzy, ti, 'linear','extrap');

figure;
grid on;
set(gcf,'Color','w');
set(gca, 'Box', 'off'); 
plot(traiectorie_ideala_fuzzy(:, 1), traiectorie_ideala_fuzzy(:, 2)); hold on;
plot(traiectorie_robot_fuzzy(:, 1), traiectorie_robot_fuzzy(:, 2));
box off; legend('Reference trajectory', 'Robot trajectory');

traiectorie_ideala_fuzzy = traiectorie_ideala_fuzzy';
traiectorie_robot_fuzzy = traiectorie_robot_fuzzy';
save("traiectorie_robot_fuzzy_ctrl.mat",...
    'traiectorie_ideala_fuzzy', 'traiectorie_robot_fuzzy');


