function dS_qdq = partial_derivative(S_q, q)

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

% compute partial derivative of S(q) w.r.t beta & teta

dS_qdteta = diff(S_q,q(1));
dS_qdbeta = diff(S_q,q(2));

dS_qdq = dS_qdteta + dS_qdbeta;

