clear; clc;

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.
% 

global Va2 Va3;
global a11 F a15 a18 a22 gama a26 a29;
Va2 = 5;
Va3 = 0;

KT = 0.023;
KE = 0.023;
Ra = 0.71;
tr = 19.7;
miu = 73/100;
tau_fr = 5.6*10^-3;
R = 0.0825;
R1 = 0.035;
L = 0.1689;
l1 = 0.18;
d = 0.03;
xm = 0;
ym = -0.07;
M = 15.5;
m = 0.35;
m1 = 0.35;

I0 = 0.3;
Ir1 = 2.144*10^-4;
Ir2 = 0.0012;
Ir3 = Ir2;
Ir = Ir2;
Ip1 = 1.072*10^-4;
Ip = 5.95*10^-4;
Cs1 = 0.004536;
Cv1 = 0.000429;
Cs = 0.3184;
Cv = 0.0444;

F = (tr*miu*KT*(Va2+Va3))/(R*Ra);

a11 = 1/(M+2*m+m1+(Ir1/R1^2)+(2*Ir/R^2));

a15 = 2*a11*(tr^2*miu*KT*KE+Ra*Cv)/(R^2*Ra);

a18 = 2*a11*(Cs+tr*miu*tau_fr)/R;

gama = (L*tr*miu*KT*(Va2-Va3))/(R*Ra);

a22 = 1/(M*(xm^2+ym^2)+I0+2*Ip+2*m*L^2+m1*l1^2+(l1^2*Ir1)/(R1^2)+(2*L^2*Ir)/(R^2));

a26 = a22*(2*L^2*tr^2*miu*KT*KE+Cv1*R^2*Ra+2*L^2*Cv*Ra)/(R^2*Ra);

a29 = a22*(2*L*tr*miu*tau_fr+Cs1*R+2*L*Cs)/R;

pas = 0.01;

eta1=0;
eta2=0;

for time=1:200
    eta1(time+1) = (a11*F-a15*eta1(time)-a18*sign(eta1(time)))*pas+eta1(time);
    acc_lin(time)=(a11*F-a15*eta1(time)-a18*sign(eta1(time)));
    eta2(time+1) = (a22*gama-a26*eta2(time)-a29*sign(eta2(time)))*pas+eta2(time);
    acc_ung(time) = (a22*gama-a26*eta2(time)-a29*sign(eta2(time)));
end

figure(1); set(gcf, 'color', 'w'); box off;
plot(eta1,'b');
hold on; title('Linear and angular velocities');
plot(eta2,'r');

figure(2); set(gcf, 'color', 'w'); box off;
plot(acc_lin,'b');
hold on; title('Linear and angular accelerations')
plot(acc_ung,'r');


