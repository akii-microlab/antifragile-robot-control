% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

% dynamic model of the mobile robot
% robot parameters :
%
% (x,y) = SRI centre integral with the robot
% (xm,ym) = coordinates of the centre of mass of the robot
% theta = orientation angle of the robot relative to the SRI of the earth [rad]
% beta = the orientation angle of the passive wheel [rad].
% fi1 = angular position of the passive wheel [rad]
% fi2 = angular position of the right-hand wheel [rad]
% fi3 = angular position of left wheel [rad]
% eta1 = linear speed of robot [rad]
% eta2 = robot angular velocity [rad]
% eta = [eta1, eta2]' ;
% M = robot mass [kg]
% m2 = mass of the right-hand wheel [kg]
% m3 = left wheel mass [kg]
% m2 = m3 = m;
% m1 = passive wheel mass [kg]
% R = active wheel radius [m]
% R1 = passive wheel radius [m]
% L = distance from active wheels to centre of SRI solid with robot [m]
% l1 = distance from the SRI centre of the robot to the rotation point of the
% of passive wheel [m]
% d = length of the moving part of the passive wheel (beaver type) [m]
% I0 = moment of inertia of the robot [kg*m^2]
% Ip1 = moment of inertia of the passive wheel [kg*m^2]
% Ir2 = moment of inertia of the spur wheel [kg*m^2]
% Ir3 = moment of inertia of left wheel [kg*m^2]
% Cs1 = Coulomb frictional torque [N*m]
% Cv1 = coefficient of vascular friction [kg*m^2/s]
% Cs = Coulomb frictional torque [N*m]
% Cv = coefficient of vascular friction [kg*m^2/s]
%
% actuator modelling --> MCC
%
% Va = motor armature voltage [V]
% ia = motor armature current [A]
% KT = torque characteristic constant
% Ke = back-EMF characteristic constant
% Ra = resistance
% La = inductance
% tr = reduction ratio
% miu = gearbox efficiency
% tau_fr = resistive, frictional torque

% KT = 0.023;
% Ke = 0.023;
% Ra = 0.71;
% tr = 19.7;
% miu = 73/100;
% tau_fr = 5.6*e-3;
% R = 0.0825;
% R1 = 0.035;
% L = 0.1689;
% l1 = 0.18;
% d = 0.03;
% xm = 0;
% ym = -0.07;
% M = 15.5;
% m = 0.35;
% m1 = 0.35;
%
% I0 = 0.3;
% Ir1 = 2.144*e-4;
% Ir2 = 0.0012;
% Ir3 = Ir2;
% Ip1 = 1.072*e-4;
% Ip = 5.95*e-4;
% Cs1 = 0.004536;
% Cv1 = 0.000429;
% Cs = 0.3184;
% Cv = 0.0444;

syms KT Ke Ra tr miu tau_fr R R1 theta beta beta_der L La l1 d xm ym M m m1 I0 real
syms Ir1 Ir2 Ir3 Ip1 Ip Cs1 Cv1 Cs Cv sign_beta_der sign_fi2_der sign_fi3_der fi2_der fi3_der ia ia2 ia3 real
syms ia2_der ia3_der eta1 eta2 theta_der real
% deduction of dynamic model matrices of the robot in state representation

% the dynamic model is determined based on the Lagrange formulation:

% M_q*q_der2 + C*q_der = A_q'*lamda+B_q*tau-Fr;
%
% q_der = S_q*eta;
%
% S_q' * A_q' = 0


% dynamic model in state representation has analytical form
%
% J_q*eta_der + g = G_q*tau-fr
% q_der = S_q*eta
%
%
% matrix S(q) satisfying q_der = S(q)*eta

S_q = [-sin(theta) 0;
    cos(theta) 0;
    0 1;
    (-sin(beta))/d (-d+l1*cos(beta))/d;
    cos(beta)/R1 -l1*sin(beta)/R1;
    1/R L/R;
    1/R -L/R;
    ];

% % rotation matrix of the SRI solid with the robot versus that of the earth
%
R_theta = [ cos(theta) sin(theta) 0;
    -sin(theta) cos(theta) 0;
    0 0 1
    ];
%
% % inertia matrix associated with the passive (beaver-type) wheel dynamics

M11 = M + 2*m+m1;
M12 = 0;
M13 = M*ym+m1*l1+m1*d*cos(beta);
M21 = 0;
M22 = M11;
M23 = M*xm+m1*d*sin(beta);
M31 = M13;
M32 = M23;
M33 = M*(xm^2+ym^2)+2*m*L^2+m1*l1^2+m1*d^2+2*m1*l1*d*cos(beta)+I0+Ip1;

M_beta = [
    M11 M12 M13;
    M21 M22 M23;
    M31 M32 M33
    ];

%
V_beta11 = m1*d*cos(beta);
V_beta21 = m1*d*sin(beta);
V_beta31 = Ip1+m1*d^2+m1*l1*d*cos(beta);

V_beta = [ V_beta11;V_beta21;V_beta31];

I_theta = [
    Ip1 0 0;
    0 Ir2 0;
    0 0 Ir3
    ];

I_beta = m1*d^2+Ip1;
%
%
% % inertia matrix associated with the dynamics of the whole robot
%
M_q11 = R_theta'*M_beta*R_theta;
M_q12 = (R_theta')*V_beta;
M_q13 = zeros(3);
M_q21 = (V_beta')*R_theta;
M_q22 = I_beta;
M_q23 = zeros(1,3);
M_q31 = zeros(3,3);
M_q32 = zeros(3,1);
M_q33 = I_theta;

M_q = [ M_q11 M_q12 M_q13; M_q21 M_q22 M_q23; M_q31 M_q32 M_q33];
%
% % matrix of centrifugal and Coriolis forces and torques

% C = 0;

% matrix of external forces and torques acting on the robot

B_q = [zeros(5,2);eye(2)];

% vector of frictional forces and torques of the robot wheels, Fr

% passive wheel

F1p = sign_beta_der; % the notation sign_beta_der = sign(beta_der) was made

F1 = Cs1*F1p+Cv1*beta_der;

% driving wheels

F2p = sign_fi2_der;% notation was made sign_fi2der = sign(fi2_der)
F3p = sign_fi3_der; % notation made sign_fi3_der = sign(fi3_der)

F2 = Cs * F2p + Cv*fi2_der;

F3 = Cs * F3p + Cv*fi3_der;

Fr = [zeros(1,3) F1 0 F2 F3]';

% dc engine modelling

% torque generated by each motor

tau_m = KT*ia;

% voltage applied to motor armature (analytical)

% neglect La

Va2 = Ra*ia2+La*ia2_der+Ke*tr*fi2_der;
Va3 = Ra*ia3+La*ia3_der+Ke*tr*fi3_der;

% neglecting the inductance La the current through the two motors is
ia2 = (Va2-Ke*tr*fi2_der)/Ra;
ia3 = (Va3-Ke*tr*fi3_der)/Ra;
% torque applied to the wheels is

tau2 = tr*miu*((KT*(Va2-Ke*tr*fi2_der))/Ra-tau_fr);
tau3 = tr*miu*((KT*(Va3-Ke*tr*fi3_der))/Ra-tau_fr);

% elements in the dynamic model can also be written as

J_q = S_q'*M_q*S_q;

G_q = S_q'*B_q;

fr = S_q'*Fr;

% calculation of partial derivatives qS_q/dq

q = [theta beta]; % restricted form of q to parameters of interest only
% beta and theta

dS_qdq = partial_derivative(S_q,q);

% the eta vector has two components

eta = [eta1 eta2]';

eta_squared = [eta1^2 eta2^2]';

q_der = S_q*eta;

C_q_der = diff(M_q)*q_der -(1/2)*partial_derivative(q_der'*M_q*q_der,q);

g = S_q'*M_q*dS_qdq*eta_squared + S_q'*C_q_der;

%  tau is the torque vector for the two driving wheels

tau = [tau2 tau3]';

% the final form of the model to be used for the monitoring step
% continuous

eta_der = (J_q)\(G_q*tau-g-fr);

eta1_der = eta_der(1,1);
eta2_der = eta_der(2,1);
