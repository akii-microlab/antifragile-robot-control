function y = trajectory_planner(~)

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

global Traiectorie PozitiaDorita perioada_esantionare profil_viteza;

v_d = profil_viteza(Traiectorie.punct,1);
av_d = profil_viteza(Traiectorie.punct,2);
w_d = profil_viteza(Traiectorie.punct,3);
aw_d = profil_viteza(Traiectorie.punct,4);

phi_d = angle_limit(w_d*perioada_esantionare + PozitiaDorita.Phi_dorit);
x_d = v_d*cos(phi_d)*perioada_esantionare + PozitiaDorita.X_dorit;
y_d = v_d*sin(phi_d)*perioada_esantionare + PozitiaDorita.Y_dorit;

PozitiaDorita.Phi_dorit = phi_d;
PozitiaDorita.X_dorit = x_d;
PozitiaDorita.Y_dorit = y_d;
Traiectorie.punct = Traiectorie.punct + 1;

y = [v_d; av_d; w_d; aw_d; x_d; y_d; phi_d];