function out=odometry(in)
global DefParametri Odometrie vv ww perioada_esantionare;

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

Nd=in(1,1);
Ne=in(2,1);


Kr=pi*DefParametri.raza_roata/DefParametri.nr_revolutii;
phi=Kr/DefParametri.raza_robot*(Nd-Ne);
DeltaS=Kr*(Nd+Ne);

delta_x = DeltaS*cos(Odometrie.Teta + phi/2);
delta_y = DeltaS*sin(Odometrie.Teta + phi/2);

vv(end+1)=sqrt(delta_x^2 + delta_y^2)/perioada_esantionare;
ww(end+1)=phi/perioada_esantionare;

X = Odometrie.poz_X + delta_x;
Y = Odometrie.poz_Y + delta_y;
Teta = angle_limit(Odometrie.Teta + phi);


Odometrie.poz_X = X;
Odometrie.poz_Y = Y;
Odometrie.Teta = Teta;

out=[vv(end); ww(end); X; Y; Teta];