clear; clc; 

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

%function init
global Traiectorie DefParametri ParametriCtrl Odometrie PozitiaDorita .... 
    perioada_esantionare profil_viteza timp_final v_c w_c

profil_viteza = load('traiectorie_demo_complexa.txt');

perioada_esantionare=0.2;

timp_final = length(profil_viteza)*...
    perioada_esantionare-perioada_esantionare;

v_c = 0.00;
w_c = 0.00;

Traiectorie = struct ('punct', 1,... % destination
    'eroare_max',    0.1);    % max error reference - current

DefParametri = struct('raza_robot', 0.30,...
    'raza_roata', 0.08,...
    'nr_revolutii', 500);

ParametriCtrl = struct('h', 0.01,...
    'timp_simulare',    100,...          % simulation duration
    'Kp',      0.84,...        % KP
    'Ki',      0.5);           % KI

Odometrie = struct('poz_X', 0,...
    'poz_Y', 0,...
    'Teta', 0);

PozitiaDorita = struct('X_dorit', 0,...
    'Y_dorit', 0,...
    'Phi_dorit', 0);

Ts=perioada_esantionare;
K_r = 4.96073;
Tp_r = 2.75434;

K_l = 4.91222;
Tp_l = 2.77088;

% DC Motor singular pertubation design
% UPDATEs
Ke = 0.823;
Ra = 0.21;
tr = 9.7;

num_right = K_r/Tp_r;
den_right = [1 1/Tp_r];

num_left = K_l/Tp_l;
den_left = [1 1/Tp_l];

Gmot_right=tf(num_right,den_right);
Gmot_left=tf(num_left,den_left);

[numz_right,denz_right]=c2dm(num_right,den_right,Ts,'zoh');
[numz_left,denz_left]=c2dm(num_left,den_left,Ts,'zoh');

% DC Motor PID control design and tuning
zeta = 1;
wn = 15;

wd = wn*sqrt(1-zeta^2);

a1=-2*exp(-zeta*wn*Ts)*cos(wd*Ts);
a2=exp(-2*zeta*wn*Ts);

a_left = numz_left(2);
b_left = -denz_left(2);

Ki_left = ((a1+a2+1)/(a_left*Ts));
Kp_left = ((a1+b_left+1)/(0.5*a_left)-Ki_left*Ts)/2;

a_right = numz_right(2);
b_right = -denz_right(2);

Ki_right = ((a1+a2+1)/(a_right*Ts));
Kp_right = ((a1+b_right+1)/(0.5*a_right)-Ki_right*Ts)/2;

numz_c_left = [(2*Kp_left+Ki_left*Ts) (Ki_left*Ts-2*Kp_left)];
denz_c_left = [2 -2];

numz_c_right = [(2*Kp_right+Ki_right*Ts) (Ki_right*Ts-2*Kp_right)];
denz_c_right = [2 -2];


