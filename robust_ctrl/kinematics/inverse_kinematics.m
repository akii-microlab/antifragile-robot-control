function W=inverse_kinematics(viteza)

% based on the work in Axenie, Cristian, and Daniela Cernega. 
% "Adaptive sliding mode controller design for mobile robot 
% fault tolerant control: Itroducing ARTEMIC." 
% 19th International Workshop on Robotics in Alpe-Adria-Danube Region 
% (RAAD 2010). IEEE, 2010.
% 
% and
% 
% Axenie, Cristian, and Razvan Solea. 
% "Real time control design for mobile robot fault tolerant control.
% IEEE/ASME International Conference on Mechatronic and Embedded Systems 
% and Applications. IEEE, 2010.

global DefParametri;

Vc=viteza(1,1);
Wc=viteza(2,1);

W=1/DefParametri.raza_roata*[1 DefParametri.raza_robot;1 -DefParametri.raza_robot]*[Vc;Wc];
