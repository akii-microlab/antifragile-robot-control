clc; clear;

load trajectory_planner_demo_circuit_complex_FDT.txt
load values_odometrie_orig.txt

Nsamples3=length(trajectory_planner_demo_circuit_complex_FDT);
Ts3 = 0.2;
sf3 = 0.001;
rR3 = 0.065;
rL3 = 0.065;
bump_dim3 = 0.007;
L3 = 0.3;
d3 = 0.06;
N3 = 500;

Xinitial3 = 0;
Yinitial3 = 0;
Tetainitial3 = 0;

Xtrueant3 = Xinitial3;
Ytrueant3 = Yinitial3;
Tetatrueant3 = Tetainitial3;

Xk_prev_ant3 = [0;0;0];
Xk_prev3 = [0;0;0];
Xk3=[0;0;0];

sigma_model3 = 0.01;
P3 = sigma_model3^2*eye(3);

sigma_x3 = 0.1;
sigma_y3 = 0.1;
sigma_teta3 = 0.1;

Q3 = [sigma_x3^2,   0,   0;
    0,    sigma_y3^2,   0;
    0,    0,   sigma_teta3^2];

sigma_meas3 = 0.01;
sigma_meas_x3 = sigma_meas3;
sigma_meas_y3 = sigma_meas3;
sigma_meas_teta3 = sigma_meas3;

R3 = sigma_meas3^2*eye(3);

Xk_buffer3 = zeros(3,Nsamples3);
Xk_buffer3(:,1) = Xk_prev3;
Z_buffer3 = zeros(3,Nsamples3);
r_buffer3 = zeros(3,Nsamples3);

for i=1:Nsamples3

    wL3(i) = values_odometrie_orig(i,1);
    wR3(i) = values_odometrie_orig(i,2);

    vRobot3(i) = (wR3(i)*rR3 + wL3(i)*rL3)/2;
    wRobot3(i) = (wR3(i)*rR3 - rL3*wL3(i))/L3;

    Tetatrue3(i) = Tetatrueant3 + Ts3*wRobot3(i);

    if (Tetatrue3(i) < -pi)

        Tetatrue3(i)  = Tetatrue3(i) + 2*pi;

    elseif (Tetatrue3(i) > pi)

        Tetatrue3(i) = Tetatrue3(i) - 2*pi;

    else

        Tetatrue3(i) = Tetatrue3(i);
    end

    Xtrue3(i) = Xtrueant3 + Ts3*(vRobot3(i)*cos(Tetatrue3(i))-d3*wRobot3(i)*sin(Tetatrue3(i)));
    Ytrue3(i) = Ytrueant3 + Ts3*(vRobot3(i)*sin(Tetatrue3(i))+d3*wRobot3(i)*cos(Tetatrue3(i)));

    Z3 = [Xtrue3(i);Ytrue3(i);Tetatrue3(i)] + sigma_meas3*randn;

    Z_buffer3(:,i) = Z3;

    Phi3 = [1, 0, Ts3*(-vRobot3(i)*sin(Tetatrue3(i))-d3*wRobot3(i)*cos(Tetatrue3(i)));
        0, 1,  Ts3*(vRobot3(i)*cos(Tetatrue3(i))-d3*wRobot3(i)*sin(Tetatrue3(i)));
        0, 0,                                                 1];

    H3= eye(3);

    P13 = Phi3*P3*Phi3' + Q3;

    S3 = H3*P13*H3' + R3;

    K3 = P13*H3'*inv(S3);

    P3 = (eye(3)-K3*H3)*P13*(eye(3)-K3*H3)'+K3*R3*K3';

    r3= Z3 - H3*Phi3*Xk_prev3;
    r_buffer3(:,i) = r3;

    Xk3 = Phi3*Xk_prev3 + K3*r3;
    Xk_buffer3(:,i) = Xk3;

    Xk_prev3 = Xk3;
    Xtrueant3 = Xtrue3(i);
    Ytrueant3 = Ytrue3(i);
    Tetatrueant3 = Tetatrue3(i);

end

for i=101:Nsamples3

    wL3(i) = values_odometrie_orig(i,1);
    wR3(i) = values_odometrie_orig(i,2);

    if(mod(i,34)==0)
        rR3 = rR3 + bump_dim3;
    end
    vRobot3(i) = (wR3(i)*rR3 + wL3(i)*rL3)/2;
    wRobot3(i) = (wR3(i)*rR3 - rL3*wL3(i))/L3;

    Tetatrue3(i) = Tetatrueant3 + Ts3*wRobot3(i);

    if (Tetatrue3(i) < -pi)

        Tetatrue3(i)  = Tetatrue3(i) + 2*pi;

    elseif (Tetatrue3(i) > pi)

        Tetatrue3(i) = Tetatrue3(i) - 2*pi;

    else

        Tetatrue3(i) = Tetatrue3(i);
    end

    Xtrue3(i) = Xtrueant3 + Ts3*(vRobot3(i)*cos(Tetatrue3(i))-d3*wRobot3(i)*sin(Tetatrue3(i)));
    Ytrue3(i) = Ytrueant3 + Ts3*(vRobot3(i)*sin(Tetatrue3(i))+d3*wRobot3(i)*cos(Tetatrue3(i)));

    Z3 = [Xtrue3(i);Ytrue3(i);Tetatrue3(i)] + sigma_meas3*randn;

    Z_buffer3(:,i) = Z3;

    Phi3 = [1, 0, Ts3*(-vRobot3(i)*sin(Tetatrue3(i))-d3*wRobot3(i)*cos(Tetatrue3(i)));
        0, 1,  Ts3*(vRobot3(i)*cos(Tetatrue3(i))-d3*wRobot3(i)*sin(Tetatrue3(i)));
        0, 0,                                                 1];

    H3= eye(3);

    P13 = Phi3*P3*Phi3' + Q3;

    S3 = H3*P13*H3' + R3;

    K3 = P13*H3'*inv(S3);

    P3 = (eye(3)-K3*H3)*P13*(eye(3)-K3*H3)'+K3*R3*K3';

    r3= Z3 - H3*Phi3*Xk_prev3;
    r_buffer3(:,i) = r3;

    Xk3 = Phi3*Xk_prev3 + K3*r3;
    Xk_buffer3(:,i) = Xk3;

    Xk_prev3 = Xk3;
    Xtrueant3 = Xtrue3(i);
    Ytrueant3 = Ytrue3(i);
    Tetatrueant3 = Tetatrue3(i);

end

figure(1);
hold on;
plot(Xtrue3,Ytrue3,'LineWidth',1.5);
xlabel('Position on X [m] ');
ylabel('Position on Y [m] ');
set(gcf, 'color', 'w'); box off;

ti = 1:70;
t = 1:length(Xk_buffer3(1,:));

xe = interp1(t, Xk_buffer3(1,:), ti);
ye = interp1(t, Xk_buffer3(2,:), ti);
phie = interp1(t, 10*Xk_buffer3(3,:), ti);

figure(2); set(gcf, 'color', 'w'); box off;
subplot(2,1,1)
plot(xe,'LineWidth',1.5); hold on;
plot(ye,'LineWidth',1.5);
legend('x_e','y_e')
xlabel('Time [s]','fontsize',12)
ylabel('Errors x_e and y_e [m]','fontsize',12)
set(gca,'fontsize',12); box off;
subplot(2,1,2)
plot(phie,'LineWidth',1.5)
legend('\phi_e')
xlabel('Time [s]','fontsize',12)
ylabel('Angular errror \phi_e [deg]','fontsize',12)
set(gca,'fontsize',12);  box off;
set(gcf, 'color', 'w'); box off;

xebump_right = Xk_buffer3(1,:);
yebump_right = Xk_buffer3(2,:);
phiebump_right = Xk_buffer3(3,:);
save('bump_right_behaviors.mat','xebump_right','yebump_right','phiebump_right');

