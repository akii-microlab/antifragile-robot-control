clc; clear;

load trajectory_planner_demo_circuit_complex_FDT.txt
load traiectorie_demo_complexa.txt
trajectory_planner_demo_circuit_complex_FDT = traiectorie_demo_complexa;
load values_odometrie_orig.txt

Nsamples1=length(trajectory_planner_demo_circuit_complex_FDT);
Ts1 = 0.2;

rR1 = 0.045;
rL1 = 0.065;
L1 = 0.3;
d1 = 0.06;
N1 = 500;

Xinitial1 = 0;
Yinitial1 = 0;
Tetainitial1 = 0;

Xtrueant1 = Xinitial1;
Ytrueant1 = Yinitial1;
Tetatrueant1 = Tetainitial1;

Xk_prev_ant1 = [0;0;0];
Xk_prev1 = [0;0;0];
Xk1=[0;0;0];

sigma_model1 = 0.01;
P1 = sigma_model1^2*eye(3);

sigma_x1 = 0.1;
sigma_y1 = 0.1;
sigma_teta1 = 0.1;

Q1 = [sigma_x1^2,   0,   0;
    0,    sigma_y1^2,   0;
    0,    0,   sigma_teta1^2];

sigma_meas1 = 0.01;
sigma_meas_x1 = sigma_meas1;
sigma_meas_y1 = sigma_meas1;
sigma_meas_teta1 = sigma_meas1;

R1= sigma_meas1^2*eye(3);

Xk_buffer1 = zeros(3,Nsamples1);
Xk_buffer1(:,1) = Xk_prev1;
Z_buffer1 = zeros(3,Nsamples1);
r_buffer1 = zeros(3,Nsamples1);

for i=1:100

    wL1(i) = values_odometrie_orig(i,1);
    wR1(i) = values_odometrie_orig(i,2);

    vRobot1(i) = (wR1(i)*rL1 + wL1(i)*rL1)/2;
    wRobot1(i) = (wR1(i)*rL1 - rL1*wL1(i))/L1;

    Tetatrue1(i) = Tetatrueant1 + Ts1*wRobot1(i);

    if (Tetatrue1(i) < -pi)

        Tetatrue1(i)  = Tetatrue1(i) + 2*pi;

    elseif (Tetatrue1(i) > pi)

        Tetatrue1(i) = Tetatrue1(i) - 2*pi;

    else

        Tetatrue1(i) = Tetatrue1(i);
    end

    Xtrue1(i) = Xtrueant1 + Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
    Ytrue1(i) = Ytrueant1 + Ts1*(vRobot1(i)*sin(Tetatrue1(i))+d1*wRobot1(i)*cos(Tetatrue1(i)));

    Z1= [Xtrue1(i);Ytrue1(i);Tetatrue1(i)] + sigma_meas1*randn;

    Z_buffer1(:,i) = Z1;

    Phi1 = [1, 0, Ts1*(-vRobot1(i)*sin(Tetatrue1(i))-d1*wRobot1(i)*cos(Tetatrue1(i)));
        0, 1,  Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
        0, 0,                                                 1];

    H1 = eye(3);

    P11 = Phi1*P1*Phi1' + Q1;

    S1 = H1*P11*H1' + R1;

    K1= P11*H1'*inv(S1);

    P1 = (eye(3)-K1*H1)*P11*(eye(3)-K1*H1)'+K1*R1*K1';

    r1 = Z1 - H1*Phi1*Xk_prev1;

    r_buffer1(:,i) = r1;
    Xk1= Phi1*Xk_prev1 + K1*r1;

    Xk_buffer1(:,i) = Xk1;

    Xk_prev1 = Xk1;
    Xtrueant1 = Xtrue1(i);
    Ytrueant1 = Ytrue1(i);
    Tetatrueant1 = Tetatrue1(i);

end
for i=101:Nsamples1

    wL1(i) = values_odometrie_orig(i,1);
    wR1(i) = values_odometrie_orig(i,2);

    vRobot1(i) = (wR1(i)*rR1 + wL1(i)*rL1)/2;
    wRobot1(i) = (wR1(i)*rR1 - rL1*wL1(i))/L1;

    Tetatrue1(i) = Tetatrueant1 + Ts1*wRobot1(i);

    if (Tetatrue1(i) < -pi)

        Tetatrue1(i)  = Tetatrue1(i) + 2*pi;

    elseif (Tetatrue1(i) > pi)

        Tetatrue1(i) = Tetatrue1(i) - 2*pi;

    else

        Tetatrue1(i) = Tetatrue1(i);
    end

    Xtrue1(i) = Xtrueant1 + Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
    Ytrue1(i) = Ytrueant1 + Ts1*(vRobot1(i)*sin(Tetatrue1(i))+d1*wRobot1(i)*cos(Tetatrue1(i)));

    Z1= [Xtrue1(i);Ytrue1(i);Tetatrue1(i)] + sigma_meas1*randn;

    Z_buffer1(:,i) = Z1;

    Phi1 = [1, 0, Ts1*(-vRobot1(i)*sin(Tetatrue1(i))-d1*wRobot1(i)*cos(Tetatrue1(i)));
        0, 1,  Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
        0, 0,                                                 1];

    H1 = eye(3);

    P11 = Phi1*P1*Phi1' + Q1;

    S1 = H1*P11*H1' + R1;

    K1= P11*H1'*inv(S1);

    P1 = (eye(3)-K1*H1)*P11*(eye(3)-K1*H1)'+K1*R1*K1';

    r1 = Z1 - H1*Phi1*Xk_prev1;

    r_buffer1(:,i) = r1;
    Xk1= Phi1*Xk_prev1 + K1*r1;

    Xk_buffer1(:,i) = Xk1;

    % actualizare
    Xk_prev1 = Xk1;
    Xtrueant1 = Xtrue1(i);
    Ytrueant1 = Ytrue1(i);
    Tetatrueant1 = Tetatrue1(i);

end
figure(1);
hold on;
plot(Xtrue1,Ytrue1,'LineWidth',1.5);
xlabel('Position on X [m] ');
ylabel('Position on Y [m] ');
set(gcf, 'color', 'w'); box off;

ti = 1:70;
t = 1:length(Xk_buffer1(1,:));

xe = interp1(t, Xk_buffer1(1,:), ti);
ye = interp1(t, Xk_buffer1(2,:), ti);
phie = interp1(t, 10*Xk_buffer1(3,:), ti);

figure(2); set(gcf, 'color', 'w'); box off;
subplot(2,1,1)
plot(xe,'LineWidth',1.5); hold on;
plot(ye,'LineWidth',1.5);
legend('x_e','y_e')
xlabel('Time [s]','fontsize',12)
ylabel('Errors x_e and y_e [m]','fontsize',12)
set(gca,'fontsize',12); box off;
subplot(2,1,2)
plot(phie,'LineWidth',1.5)
legend('\phi_e')
xlabel('Time [s]','fontsize',12)
ylabel('Angular errror \phi_e [deg]','fontsize',12)
set(gca,'fontsize',12);  box off;

xeflat_right = Xk_buffer1(1,:);
yeflat_right = Xk_buffer1(2,:);
phieflat_right = Xk_buffer1(3,:);
save('flat_right_behaviors.mat','xeflat_right','yeflat_right','phieflat_right');
