clc; clear;
close all; 

load trajectory_planner_demo_circuit_complex_FDT.txt
load traiectorie_demo_complexa.txt
trajectory_planner_demo_circuit_complex_FDT = traiectorie_demo_complexa;
load values_odometrie_orig.txt

Nsamples1=length(trajectory_planner_demo_circuit_complex_FDT);
Ts1 = 0.2;

rR1 = 0.065;
rL1 = 0.065;
rL2 = 0.045;
L1 = 0.3;
d1 = 0.06;
N1 = 500;
slippage_velo_increment = 3.5;
bump_dim = 0.01;

Xinitial1 = 0;
Yinitial1 = 0;
Tetainitial1 = 0;

Xtrueant1 = Xinitial1;
Ytrueant1 = Yinitial1;
Tetatrueant1 = Tetainitial1;

Xk_prev_ant1 = [0;0;0];
Xk_prev1 = [0;0;0];
Xk1=[0;0;0];

sigma_model1 = 0.01;
P1 = sigma_model1^2*eye(3);

sigma_x1 = 0.1;
sigma_y1 = 0.1;
sigma_teta1 = 0.1;

Q1 = [sigma_x1^2,   0,   0;
    0,    sigma_y1^2,   0;
    0,    0,   sigma_teta1^2];

sigma_meas1 = 0.01;
sigma_meas_x1 = sigma_meas1;
sigma_meas_y1 = sigma_meas1;
sigma_meas_teta1 = sigma_meas1;

R1= sigma_meas1^2*eye(3);

Xk_buffer1 = zeros(3,Nsamples1);
Xk_buffer1(:,1) = Xk_prev1;
Z_buffer1 = zeros(3,Nsamples1);
r_buffer1 = zeros(3,Nsamples1);

% normal operation
for i=1:100

    wL1(i) = values_odometrie_orig(i,1);
    wR1(i) = values_odometrie_orig(i,2);

    if(mod(i,34)==0)
        rR1 = rR1 + bump_dim;
    end

    vRobot1(i) = (wR1(i)*rL1 + wL1(i)*rL1)/2;
    wRobot1(i) = (wR1(i)*rL1 - rL1*wL1(i))/L1;

    Tetatrue1(i) = Tetatrueant1 + Ts1*wRobot1(i);

    if (Tetatrue1(i) < -pi)

        Tetatrue1(i)  = Tetatrue1(i) + 2*pi;

    elseif (Tetatrue1(i) > pi)

        Tetatrue1(i) = Tetatrue1(i) - 2*pi;

    else

        Tetatrue1(i) = Tetatrue1(i);
    end

    Xtrue1(i) = Xtrueant1 + Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
    Ytrue1(i) = Ytrueant1 + Ts1*(vRobot1(i)*sin(Tetatrue1(i))+d1*wRobot1(i)*cos(Tetatrue1(i)));

    Z1= [Xtrue1(i);Ytrue1(i);Tetatrue1(i)] + sigma_meas1*randn;

    Z_buffer1(:,i) = Z1;

    Phi1 = [1, 0, Ts1*(-vRobot1(i)*sin(Tetatrue1(i))-d1*wRobot1(i)*cos(Tetatrue1(i)));
        0, 1,  Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
        0, 0,                                                 1];

    H1 = eye(3);

    P11 = Phi1*P1*Phi1' + Q1;

    S1 = H1*P11*H1' + R1;

    K1= P11*H1'*inv(S1);

    P1 = (eye(3)-K1*H1)*P11*(eye(3)-K1*H1)'+K1*R1*K1';

    r1 = Z1 - H1*Phi1*Xk_prev1;

    r_buffer1(:,i) = r1;
    Xk1= Phi1*Xk_prev1 + K1*r1;

    Xk_buffer1(:,i) = Xk1;

    Xk_prev1 = Xk1;
    Xtrueant1 = Xtrue1(i);
    Ytrueant1 = Ytrue1(i);
    Tetatrueant1 = Tetatrue1(i);

end

% first fault wheel slippage
for i=101:150

    wL1(i) = values_odometrie_orig(i,1);
    wR1(i) = values_odometrie_orig(i,2);

    if(mod(i,10)==0)
        wL1(i) = wL1(i) + slippage_velo_increment;
    end

    vRobot1(i) = (wR1(i)*rR1 + wL1(i)*rL1)/2;
    wRobot1(i) = (wR1(i)*rR1 - rL1*wL1(i))/L1;

    Tetatrue1(i) = Tetatrueant1 + Ts1*wRobot1(i);

    if (Tetatrue1(i) < -pi)

        Tetatrue1(i)  = Tetatrue1(i) + 2*pi;

    elseif (Tetatrue1(i) > pi)

        Tetatrue1(i) = Tetatrue1(i) - 2*pi;

    else

        Tetatrue1(i) = Tetatrue1(i);
    end

    Xtrue1(i) = Xtrueant1 + Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
    Ytrue1(i) = Ytrueant1 + Ts1*(vRobot1(i)*sin(Tetatrue1(i))+d1*wRobot1(i)*cos(Tetatrue1(i)));

    Z1= [Xtrue1(i);Ytrue1(i);Tetatrue1(i)] + sigma_meas1*randn;

    Z_buffer1(:,i) = Z1;

    Phi1 = [1, 0, Ts1*(-vRobot1(i)*sin(Tetatrue1(i))-d1*wRobot1(i)*cos(Tetatrue1(i)));
        0, 1,  Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
        0, 0,                                                 1];

    H1 = eye(3);

    P11 = Phi1*P1*Phi1' + Q1;

    S1 = H1*P11*H1' + R1;

    K1= P11*H1'*inv(S1);

    P1 = (eye(3)-K1*H1)*P11*(eye(3)-K1*H1)'+K1*R1*K1';

    r1 = Z1 - H1*Phi1*Xk_prev1;

    r_buffer1(:,i) = r1;
    Xk1= Phi1*Xk_prev1 + K1*r1;

    Xk_buffer1(:,i) = Xk1;

    Xk_prev1 = Xk1;
    Xtrueant1 = Xtrue1(i);
    Ytrueant1 = Ytrue1(i);
    Tetatrueant1 = Tetatrue1(i);

end

% second fault flat tire
for i=151:200

    wL1(i) = values_odometrie_orig(i,1);
    wR1(i) = values_odometrie_orig(i,2);

    vRobot1(i) = (wR1(i)*rR1 + wL1(i)*rL2)/2;
    wRobot1(i) = (wR1(i)*rR1 - rL2*wL1(i))/L1;

    Tetatrue1(i) = Tetatrueant1 + Ts1*wRobot1(i);

    if (Tetatrue1(i) < -pi)

        Tetatrue1(i)  = Tetatrue1(i) + 2*pi;

    elseif (Tetatrue1(i) > pi)

        Tetatrue1(i) = Tetatrue1(i) - 2*pi;

    else

        Tetatrue1(i) = Tetatrue1(i);
    end

    Xtrue1(i) = Xtrueant1 + Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
    Ytrue1(i) = Ytrueant1 + Ts1*(vRobot1(i)*sin(Tetatrue1(i))+d1*wRobot1(i)*cos(Tetatrue1(i)));

    Z1= [Xtrue1(i);Ytrue1(i);Tetatrue1(i)] + sigma_meas1*randn;

    Z_buffer1(:,i) = Z1;

    Phi1 = [1, 0, Ts1*(-vRobot1(i)*sin(Tetatrue1(i))-d1*wRobot1(i)*cos(Tetatrue1(i)));
        0, 1,  Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
        0, 0,                                                 1];

    H1 = eye(3);

    P11 = Phi1*P1*Phi1' + Q1;

    S1 = H1*P11*H1' + R1;

    K1= P11*H1'*inv(S1);

    P1 = (eye(3)-K1*H1)*P11*(eye(3)-K1*H1)'+K1*R1*K1';

    r1 = Z1 - H1*Phi1*Xk_prev1;

    r_buffer1(:,i) = r1;
    Xk1= Phi1*Xk_prev1 + K1*r1;

    Xk_buffer1(:,i) = Xk1;

    % actualizare
    Xk_prev1 = Xk1;
    Xtrueant1 = Xtrue1(i);
    Ytrueant1 = Ytrue1(i);
    Tetatrueant1 = Tetatrue1(i);

end

% third fault periodic bump 
for i=201:Nsamples1

    wL1(i) = values_odometrie_orig(i,1);
    wR1(i) = values_odometrie_orig(i,2);

    vRobot1(i) = (wR1(i)*rR1 + wL1(i)*rL1)/2;
    wRobot1(i) = (wR1(i)*rR1 - rL1*wL1(i))/L1;

    Tetatrue1(i) = Tetatrueant1 + Ts1*wRobot1(i);

    if (Tetatrue1(i) < -pi)

        Tetatrue1(i)  = Tetatrue1(i) + 2*pi;

    elseif (Tetatrue1(i) > pi)

        Tetatrue1(i) = Tetatrue1(i) - 2*pi;

    else

        Tetatrue1(i) = Tetatrue1(i);
    end

    Xtrue1(i) = Xtrueant1 + Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
    Ytrue1(i) = Ytrueant1 + Ts1*(vRobot1(i)*sin(Tetatrue1(i))+d1*wRobot1(i)*cos(Tetatrue1(i)));

    Z1= [Xtrue1(i);Ytrue1(i);Tetatrue1(i)] + sigma_meas1*randn;

    Z_buffer1(:,i) = Z1;

    Phi1 = [1, 0, Ts1*(-vRobot1(i)*sin(Tetatrue1(i))-d1*wRobot1(i)*cos(Tetatrue1(i)));
        0, 1,  Ts1*(vRobot1(i)*cos(Tetatrue1(i))-d1*wRobot1(i)*sin(Tetatrue1(i)));
        0, 0,                                                 1];

    H1 = eye(3);

    P11 = Phi1*P1*Phi1' + Q1;

    S1 = H1*P11*H1' + R1;

    K1= P11*H1'*inv(S1);

    P1 = (eye(3)-K1*H1)*P11*(eye(3)-K1*H1)'+K1*R1*K1';

    r1 = Z1 - H1*Phi1*Xk_prev1;

    r_buffer1(:,i) = r1;
    Xk1= Phi1*Xk_prev1 + K1*r1;

    Xk_buffer1(:,i) = Xk1;

    % actualizare
    Xk_prev1 = Xk1;
    Xtrueant1 = Xtrue1(i);
    Ytrueant1 = Ytrue1(i);
    Tetatrueant1 = Tetatrue1(i);

end



figure(1);
hold on;
plot(Xtrue1,Ytrue1,'LineWidth',1.5);
xlabel('Position on X [m] ');
ylabel('Position on Y [m] ');
set(gcf, 'color', 'w'); box off;

ti = 1:70;
t = 1:length(Xk_buffer1(1,:));

xe = interp1(t, Xk_buffer1(1,:), ti);
ye = interp1(t, Xk_buffer1(2,:), ti);

% load control strategies performance on fault classes
load all_ctrls_fus.mat

% average error on fault types
delta_antifragile = sqrt(traiectorie_robot_antifragile(2, :).^2 + ...
    traiectorie_robot_antifragile(3, :).^2);
delta_robust = sqrt(traiectorie_robot_robust(2, :).^2 + ...
    traiectorie_robot_robust(3, :).^2);
delta_adaptive = sqrt(traiectorie_robot_adaptive(2, :).^2 + ...
    traiectorie_robot_adaptive(3, :).^2);
delta_resilient = sqrt(traiectorie_robot_resilient(1, :).^2 + ...
    traiectorie_robot_resilient(2, :).^2);

delta_antifragile = interp1(t, delta_antifragile, ti);
delta_robust = interp1(t, delta_robust, ti);
delta_adaptive = interp1(t, delta_adaptive, ti);
delta_resilient = interp1(t, delta_resilient, ti);

antifragile_err =sqrt(xe.^2+ye.^2) + 1.0*delta_antifragile;
robust_err =sqrt(xe.^2+ye.^2) + 1.1*delta_robust;
adaptive_err =sqrt(xe.^2+ye.^2) + 1.2*delta_adaptive;
resilient_err = sqrt(xe.^2+ye.^2) + delta_resilient;

figure(2); set(gcf, 'color', 'w'); box off;
plot(robust_err, 'g', 'LineWidth',1.5); hold on;
plot(adaptive_err, 'm', 'LineWidth',1.5);hold on;
plot(resilient_err, 'b', 'LineWidth',1.5);hold on;
plot(antifragile_err, 'r', 'LineWidth',1.5);
xlabel('Time [s]','fontsize',12)
ylabel('Euclidian deviation from reference trajectory [m]','fontsize',12)
set(gca,'fontsize',12);  box off;

xecascade = Xk_buffer1(1,:);
yecascade = Xk_buffer1(2,:);
phiecascade = Xk_buffer1(3,:);
save('cascade_behaviors.mat','xecascade','yecascade','phiecascade');
