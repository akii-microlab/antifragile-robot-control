clc; clear;

load trajectory_planner_demo_circuit_complex_FDT.txt
load values_odometrie_orig.txt

Nsamples4=length(trajectory_planner_demo_circuit_complex_FDT);
Ts4 = 0.2;
sf4 = 0.001;
rR4 = 0.065;
rL4 = 0.065;
slippage_velo_increment = 3.5;
L4 = 0.3;
d4 = 0.06;
N4 = 500;

Xinitial4 = 0;
Yinitial4 = 0;
Tetainitial4 = 0;

Xtrueant4 = Xinitial4;
Ytrueant4 = Yinitial4;
Tetatrueant4 = Tetainitial4;

Xk_prev_ant4 = [0;0;0];
Xk_prev4 = [0;0;0];
Xk4=[0;0;0];

sigma_model4 = 0.01;
P4 = sigma_model4^2*eye(3);

sigma_x4 = 0.1;
sigma_y4 = 0.1;
sigma_teta4 = 0.1;

Q4 = [sigma_x4^2,   0,   0;
    0,    sigma_y4^2,   0;
    0,    0,   sigma_teta4^2];

sigma_meas4 = 0.01;
sigma_meas_x4 = sigma_meas4;
sigma_meas_y4 = sigma_meas4;
sigma_meas_teta4 = sigma_meas4;

R4= sigma_meas4^2*eye(3);

Xk_buffer4 = zeros(3,Nsamples4);
Xk_buffer4(:,1) = Xk_prev4;
Z_buffer4 = zeros(3,Nsamples4);
r_buffer4 = zeros(3,Nsamples4);

for i=1:100

    wL4(i) = values_odometrie_orig(i,1);
    wR4(i) = values_odometrie_orig(i,2);

    vRobot4(i) = (wR4(i)*rR4 + wL4(i)*rL4)/2;
    wRobot4(i) = (wR4(i)*rR4 - rL4*wL4(i))/L4;

    Tetatrue4(i) = Tetatrueant4 + Ts4*wRobot4(i);

    if (Tetatrue4(i) < -pi)

        Tetatrue4(i)  = Tetatrue4(i) + 2*pi;

    elseif (Tetatrue4(i) > pi)

        Tetatrue4(i) = Tetatrue4(i) - 2*pi;

    else

        Tetatrue4(i) = Tetatrue4(i);
    end

    Xtrue4(i) = Xtrueant4 + Ts4*(vRobot4(i)*cos(Tetatrue4(i))-d4*wRobot4(i)*sin(Tetatrue4(i)));
    Ytrue4(i) = Ytrueant4 + Ts4*(vRobot4(i)*sin(Tetatrue4(i))+d4*wRobot4(i)*cos(Tetatrue4(i)));

    Z4 = [Xtrue4(i);Ytrue4(i);Tetatrue4(i)] + sigma_meas4*randn;

    Z_buffer4(:,i) = Z4;

    Phi4 = [1, 0, Ts4*(-vRobot4(i)*sin(Tetatrue4(i))-d4*wRobot4(i)*cos(Tetatrue4(i)));
        0, 1,  Ts4*(vRobot4(i)*cos(Tetatrue4(i))-d4*wRobot4(i)*sin(Tetatrue4(i)));
        0, 0,                                                 1];

    H4 = eye(3);

    P14 = Phi4*P4*Phi4' + Q4;

    S4 = H4*P14*H4' + R4;

    K4 = P14*H4'*inv(S4);

    P4 = (eye(3)-K4*H4)*P14*(eye(3)-K4*H4)'+K4*R4*K4';

    r4 = Z4- H4*Phi4*Xk_prev4;
    r_buffer4(:,i) = r4;

    Xk4 = Phi4*Xk_prev4 + K4*r4;
    Xk_buffer4(:,i) = Xk4;

    Xk_prev4= Xk4;
    Xtrueant4 = Xtrue4(i);
    Ytrueant4 = Ytrue4(i);
    Tetatrueant4 = Tetatrue4(i);

end

for i=101:Nsamples4

    wL4(i) = values_odometrie_orig(i,1);
    wR4(i) = values_odometrie_orig(i,2);

    if(mod(i,10)==0)
        wL4(i) = wL4(i) + slippage_velo_increment;
    end
    vRobot4(i) = (wR4(i)*rR4 + wL4(i)*rL4)/2;
    wRobot4(i) = (wR4(i)*rR4 - rL4*wL4(i))/L4;

    Tetatrue4(i) = Tetatrueant4 + Ts4*wRobot4(i);

    if (Tetatrue4(i) < -pi)

        Tetatrue4(i)  = Tetatrue4(i) + 2*pi;

    elseif (Tetatrue4(i) > pi)

        Tetatrue4(i) = Tetatrue4(i) - 2*pi;

    else

        Tetatrue4(i) = Tetatrue4(i);
    end

    Xtrue4(i) = Xtrueant4 + Ts4*(vRobot4(i)*cos(Tetatrue4(i))-d4*wRobot4(i)*sin(Tetatrue4(i)));
    Ytrue4(i) = Ytrueant4 + Ts4*(vRobot4(i)*sin(Tetatrue4(i))+d4*wRobot4(i)*cos(Tetatrue4(i)));

    Z4 = [Xtrue4(i);Ytrue4(i);Tetatrue4(i)] + sigma_meas4*randn;

    Z_buffer4(:,i) = Z4;

    Phi4 = [1, 0, Ts4*(-vRobot4(i)*sin(Tetatrue4(i))-d4*wRobot4(i)*cos(Tetatrue4(i)));
        0, 1,  Ts4*(vRobot4(i)*cos(Tetatrue4(i))-d4*wRobot4(i)*sin(Tetatrue4(i)));
        0, 0,                                                 1];

    H4 = eye(3);

    P14 = Phi4*P4*Phi4' + Q4;

    S4 = H4*P14*H4' + R4;

    K4 = P14*H4'*inv(S4);

    P4 = (eye(3)-K4*H4)*P14*(eye(3)-K4*H4)'+K4*R4*K4';

    r4 = Z4- H4*Phi4*Xk_prev4;
    r_buffer4(:,i) = r4;

    Xk4 = Phi4*Xk_prev4 + K4*r4;
    Xk_buffer4(:,i) = Xk4;

    Xk_prev4= Xk4;
    Xtrueant4 = Xtrue4(i);
    Ytrueant4 = Ytrue4(i);
    Tetatrueant4 = Tetatrue4(i);

end

min_r_ff_x = min(r_buffer4(1,:));
max_r_ff_x = max(r_buffer4(1,:));

min_r_ff_y = min(r_buffer4(2,:));
max_r_ff_y = max(r_buffer4(2,:));

min_r_ff_teta = min(r_buffer4(3,:));
max_r_ff_teta = max(r_buffer4(3,:));

figure(1);
hold on;
plot(Xtrue4,Ytrue4,'LineWidth',1.5);
xlabel('Position on X [m] ');
ylabel('Position on Y [m] ');
set(gcf, 'color', 'w'); box off;

ti = 1:70;
t = 1:length(Xk_buffer4(1,:));

xe = interp1(t, Xk_buffer4(1,:), ti);
ye = interp1(t, Xk_buffer4(2,:), ti);
phie = interp1(t, 10*Xk_buffer4(3,:), ti);

figure(2); set(gcf, 'color', 'w'); box off;
subplot(2,1,1)
plot(xe,'LineWidth',1.5); hold on;
plot(ye,'LineWidth',1.5);
legend('x_e','y_e')
xlabel('Time [s]','fontsize',12)
ylabel('Errors x_e and y_e [m]','fontsize',12)
set(gca,'fontsize',12); box off;
subplot(2,1,2)
plot(phie,'LineWidth',1.5)
legend('\phi_e')
xlabel('Time [s]','fontsize',12)
ylabel('Angular errror \phi_e [deg]','fontsize',12)
set(gca,'fontsize',12);  box off;

xeslippage_left = Xk_buffer4(1,:);
yeslippage_left = Xk_buffer4(2,:);
phieslippage_left = Xk_buffer4(3,:);
save('slippage_left_behaviors.mat','xeslippage_left','yeslippage_left','phieslippage_left');
