clc; clear;

load trajectory_planner_demo_circuit_complex_FDT.txt
load values_odometrie_orig.txt

Nsamples2=length(trajectory_planner_demo_circuit_complex_FDT);
Ts2 = 0.2;
rR2 = 0.065;
rL2 = 0.045;
L2 = 0.3;
d2 = 0.06;
N2 = 500;

Xinitial2 = 0;
Yinitial2 = 0;
Tetainitial2 = 0;

Xtrueant2 = Xinitial2;
Ytrueant2 = Yinitial2;
Tetatrueant2 = Tetainitial2;

Xk_prev_ant2 = [0;0;0];
Xk_prev2 = [0;0;0];
Xk2=[0;0;0];

sigma_model2 = 0.01;
P2 = sigma_model2^2*eye(3);

sigma_x2 = 0.1;
sigma_y2 = 0.1;
sigma_teta2 = 0.1;

Q2 = [sigma_x2^2,   0,   0;
    0,    sigma_y2^2,  0;
    0,    0,   sigma_teta2^2];

sigma_meas2 = 0.01;
sigma_meas_x2 = sigma_meas2;
sigma_meas_y2= sigma_meas2;
sigma_meas_teta2 = sigma_meas2;

R2 = sigma_meas2^2*eye(3);

Xk_buffer2 = zeros(3,Nsamples2);
Xk_buffer2(:,1) = Xk_prev2;
Z_buffer2 = zeros(3,Nsamples2);
r_buffer2 = zeros(3,Nsamples2);

for i=1:100

    wL2(i) = values_odometrie_orig(i,1);
    wR2(i) = values_odometrie_orig(i,2);

    vRobot2(i) = (wR2(i)*rR2 + wL2(i)*rR2)/2;
    wRobot2(i) = (wR2(i)*rR2 - rR2*wL2(i))/L2;

    Tetatrue2(i) = Tetatrueant2 + Ts2*wRobot2(i);

    if (Tetatrue2(i) < -pi)

        Tetatrue2(i)  = Tetatrue2(i) + 2*pi;

    elseif (Tetatrue2(i) > pi)

        Tetatrue2(i) = Tetatrue2(i) - 2*pi;

    else

        Tetatrue2(i) = Tetatrue2(i);
    end

    Xtrue2(i) = Xtrueant2 + Ts2*(vRobot2(i)*cos(Tetatrue2(i))-d2*wRobot2(i)*sin(Tetatrue2(i)));
    Ytrue2(i) = Ytrueant2 + Ts2*(vRobot2(i)*sin(Tetatrue2(i))+d2*wRobot2(i)*cos(Tetatrue2(i)));

    Z2 = [Xtrue2(i);Ytrue2(i);Tetatrue2(i)] + sigma_meas2*randn;

    Z_buffer2(:,i) = Z2;
    Phi2 = [1, 0, Ts2*(-vRobot2(i)*sin(Tetatrue2(i))-d2*wRobot2(i)*cos(Tetatrue2(i)));
        0, 1,  Ts2*(vRobot2(i)*cos(Tetatrue2(i))-d2*wRobot2(i)*sin(Tetatrue2(i)));
        0, 0,                                                 1];

    H2 = eye(3);

    P12 = Phi2*P2*Phi2' + Q2;

    S2 = H2*P12*H2' + R2;

    K2 = P12*H2'*inv(S2);

    P2 = (eye(3)-K2*H2)*P12*(eye(3)-K2*H2)'+K2*R2*K2';

    r2 = Z2 - H2*Phi2*Xk_prev2;

    r_buffer2(:,i) = r2;

    Xk2 = Phi2*Xk_prev2 + K2*r2;

    Xk_buffer2(:,i) = Xk2;

    Xk_prev2 = Xk2;
    Xtrueant2 = Xtrue2(i);
    Ytrueant2 = Ytrue2(i);
    Tetatrueant2 = Tetatrue2(i);

end

for i=101:Nsamples2

    wL2(i) = values_odometrie_orig(i,1);
    wR2(i) = values_odometrie_orig(i,2);

    vRobot2(i) = (wR2(i)*rR2 + wL2(i)*rL2)/2;
    wRobot2(i) = (wR2(i)*rR2 - rL2*wL2(i))/L2;

    Tetatrue2(i) = Tetatrueant2 + Ts2*wRobot2(i);

    if (Tetatrue2(i) < -pi)

        Tetatrue2(i)  = Tetatrue2(i) + 2*pi;

    elseif (Tetatrue2(i) > pi)

        Tetatrue2(i) = Tetatrue2(i) - 2*pi;

    else

        Tetatrue2(i) = Tetatrue2(i);
    end

    Xtrue2(i) = Xtrueant2 + Ts2*(vRobot2(i)*cos(Tetatrue2(i))-d2*wRobot2(i)*sin(Tetatrue2(i)));
    Ytrue2(i) = Ytrueant2 + Ts2*(vRobot2(i)*sin(Tetatrue2(i))+d2*wRobot2(i)*cos(Tetatrue2(i)));

    Z2 = [Xtrue2(i);Ytrue2(i);Tetatrue2(i)] + sigma_meas2*randn;

    Z_buffer2(:,i) = Z2;

    Phi2 = [1, 0, Ts2*(-vRobot2(i)*sin(Tetatrue2(i))-d2*wRobot2(i)*cos(Tetatrue2(i)));
        0, 1,  Ts2*(vRobot2(i)*cos(Tetatrue2(i))-d2*wRobot2(i)*sin(Tetatrue2(i)));
        0, 0,                                                 1];

    H2 = eye(3);

    P12 = Phi2*P2*Phi2' + Q2;

    S2 = H2*P12*H2' + R2;

    K2 = P12*H2'*inv(S2);

    P2 = (eye(3)-K2*H2)*P12*(eye(3)-K2*H2)'+K2*R2*K2';

    r2 = Z2 - H2*Phi2*Xk_prev2;

    r_buffer2(:,i) = r2;

    Xk2 = Phi2*Xk_prev2 + K2*r2;

    Xk_buffer2(:,i) = Xk2;

    Xk_prev2 = Xk2;
    Xtrueant2 = Xtrue2(i);
    Ytrueant2 = Ytrue2(i);
    Tetatrueant2 = Tetatrue2(i);

end

figure(1);
hold on;
plot(Xtrue2,Ytrue2,'LineWidth',1.5);
xlabel('Position on X [m] ');
ylabel('Position on Y [m] ');
set(gcf, 'color', 'w'); box off;

ti = 1:70;
t = 1:length(Xk_buffer2(1,:));

xe = interp1(t, Xk_buffer2(1,:), ti);
ye = interp1(t, Xk_buffer2(2,:), ti);
phie = interp1(t, 10*Xk_buffer2(3,:), ti);

figure(2); set(gcf, 'color', 'w'); box off;
subplot(2,1,1)
plot(xe,'LineWidth',1.5); hold on;
plot(ye,'LineWidth',1.5);
legend('x_e','y_e')
xlabel('Time [s]','fontsize',12)
ylabel('Errors x_e and y_e [m]','fontsize',12)
set(gca,'fontsize',12); box off;
subplot(2,1,2)
plot(phie,'LineWidth',1.5)
legend('\phi_e')
xlabel('Time [s]','fontsize',12)
ylabel('Angular errror \phi_e [deg]','fontsize',12)
set(gca,'fontsize',12);  box off;

xeflat_left = Xk_buffer2(1,:);
yeflat_left = Xk_buffer2(2,:);
phieflat_left = Xk_buffer2(3,:);
save('flat_left_behaviors.mat','xeflat_left','yeflat_left','phieflat_left');
