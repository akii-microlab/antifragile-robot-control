syms Xtrue Ytrue Tetatrue Ts vRobot wRobot wR wL rR rL d L dt lsamp rsamp 

state = [Xtrue;Ytrue;Tetatrue];

Kr=pi*((rR+rL)/2)/N;
phi=Kr/Rrob*(rsamp - lsamp);
DeltaS=Kr*(rsamp + lsamp);

delta_x = DeltaS*cos(Tetatrue + phi/2);
delta_y = DeltaS*sin(Tetatrue + phi/2);

fx = Xtrue + delta_x;
fy = Ytrue + delta_y;
fteta = Tetatrue + phi;

f = [fx;fy;fteta];

Phi = jacobian(f,state);

Z = [Xtrue;Ytrue;Tetatrue] + sigma_meas*randn;

H = jacobian(Z,state);
