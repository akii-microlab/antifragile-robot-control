function draw=robot_model(data)

% based on the work of Solea, Razvan, and Urbano Nunes. 
% "Trajectory planning with velocity planner for fully-automated 
% passenger vehicles." 2006 
% IEEE Intelligent Transportation Systems Conference. IEEE, 2006.

L = 0.61;
Lm = 1;
Lc = 0.2;
draw=[];

for i=1:length(data(:,1))
    
    x0 = data(i,1);
    y0 = data(i,2);
    theta0 = data(i,3);
    
    xc = x0-Lc*cos(theta0);
    yc = y0-Lc*sin(theta0);
        
    x1 = xc+L/2*sin(theta0);
    y1 = yc-L/2*cos(theta0);

    x4 = xc-L/2*sin(theta0);
    y4 = yc+L/2*cos(theta0);

    x5 = xc+Lm*cos(theta0);
    y5 = yc+Lm*sin(theta0);

    x2 = x5+L/2*sin(theta0);
    y2 = y5-L/2*cos(theta0);

    x3 = x5-L/2*sin(theta0);
    y3 = y5+L/2*cos(theta0);

    A=[x1 x2 x3 x4 x1 x5 x4;y1 y2 y3 y4 y1 y5 y4]';
    draw=[draw; A];
end