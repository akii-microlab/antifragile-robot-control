function v=compute_final_velocity(~,~,~,~,L,t_fin,vi,vf,ai,af,~,l,KK,~,~)

% based on the work of Solea, Razvan, and Urbano Nunes. 
% "Trajectory planning with velocity planner for fully-automated 
% passenger vehicles." 2006 
% IEEE Intelligent Transportation Systems Conference. IEEE, 2006.

t_gama=round2(t_fin/5);

t1=t_gama;
t2=t1;
t4=t2;
t5=t4;
t3=t_fin-t1-t2-t4-t5;
t=round2([t1 t2 t3 t4 t5]);

%find a starting solution
v=find_starting_solution(L,vi,vf,ai,af,t);
v2=v(1);
v3=v(2);
a2=v(3);

aa=plot_quintic_equations(vi,ai,vf,af,v2,v3,a2,t,L);
t_fin=t(1)+t(2)+t(3)+t(4)+t(5);

% interpolate
l_init=interp1(0:0.01:1,l,0:0.01/t_fin:1,'spline');
l_init(length(l_init))=L;
KK_init=interp1(0:0.01:1,KK,0:0.01/t_fin:1,'spline');
tt2=interp1(aa(4,:),aa(1,:),l_init,'spline');
speed=interp1(aa(1,:),aa(2,:),tt2,'spline');
omega3=KK_init.*speed;
omega4=interp1(tt2,omega3,0:0.01:t_fin,'spline');

% final velocity
v=[aa;omega4;KK_init];

