% based on the work of Solea, Razvan, and Urbano Nunes. 
% "Trajectory planning with velocity planner for fully-automated 
% passenger vehicles." 2006 
% IEEE Intelligent Transportation Systems Conference. IEEE, 2006.

clear; clc;
global kkkk xxx yyy teta P v_final;

% paper trajectory 
xxx = [0, 0.5, 1, 1.5, 2, 2.5, 3, 2.5, 2, 2.5, 3,3.5, 4.0, 3.5,...
    3.0,2.5,2.0, 1.5, 1, 0.5, 0.0];
yyy = [0, 0, 0.5, 1, 0.5, 0, 0.5, 1, 0.5, 0,0,0,0.5,1,1,1,1, 1,...
    0.5, 0.0, 0.0];
teta = [0, 0, pi/2, 0, -pi/2, 0, pi/2, pi, -pi/2, 0, 0,0, pi/2,...
    pi,pi,pi,pi, pi, -pi/2, -pi, -pi];

% line trajectory 
% xxx = [0, 2];
% yyy = [0, 0];
% teta =[0, 0];

% _O_ trajectory 
% xxx = [3.9, 4, 4.75, 4, 3.25, 4, 4.2];
% yyy = [0, 0, 0.75, 1.5, 0.75, 0, 0];
% teta = [0, 0, pi/2, pi, -pi/2, 0, 0];

P = [xxx;yyy];
kkkk=[zeros(2,length(P))];
v_final=[zeros(1,length(P))];
quintic_eq_path_planner_velo_acc(v_final);
