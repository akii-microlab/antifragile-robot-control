load all_ctrls_fus.mat;
load bump_left_behaviors.mat;
load bump_right_behaviors.mat;
load flat_left_behaviors.mat;
load flat_right_behaviors.mat;

% adaptive control analysis
bump_left_adaptive_xe = xebump_left - traiectorie_robot_adaptive(2, :);
bump_left_adaptive_ye = yebump_left - traiectorie_robot_adaptive(3, :);
bump_left_adaptive_phie = phiebump_left - traiectorie_robot_adaptive(4, :);

bump_right_adaptive_xe = xebump_right - traiectorie_robot_adaptive(2, :);
bump_right_adaptive_ye = yebump_right - traiectorie_robot_adaptive(3, :);
bump_right_adaptive_phie = phiebump_right - traiectorie_robot_adaptive(4, :);

flat_left_adaptive_xe = xeflat_left - traiectorie_robot_adaptive(2, :);
flat_left_adaptive_ye = yeflat_left - traiectorie_robot_adaptive(3, :);
flat_left_adaptive_phie = phieflat_left - traiectorie_robot_adaptive(4, :);

flat_right_adaptive_xe = xeflat_left - traiectorie_robot_adaptive(2, :);
flat_right_adaptive_ye = yeflat_right - traiectorie_robot_adaptive(3, :);
flat_right_adaptive_phie = phieflat_right - traiectorie_robot_adaptive(4, :);


% robust control analysis
bump_left_robust_xe = xebump_left - traiectorie_robot_robust(2, :);
bump_left_robust_ye = yebump_left - traiectorie_robot_robust(3, :);
bump_left_robust_phie = phiebump_left - traiectorie_robot_robust(4, :);

bump_right_robust_xe = xebump_right - traiectorie_robot_robust(2, :);
bump_right_robust_ye = yebump_right - traiectorie_robot_robust(3, :);
bump_right_robust_phie = phiebump_right - traiectorie_robot_robust(4, :);

flat_left_robust_xe = xeflat_left - traiectorie_robot_robust(2, :);
flat_left_robust_ye = yeflat_left - traiectorie_robot_robust(3, :);
flat_left_robust_phie = phieflat_left - traiectorie_robot_robust(4, :);

flat_right_robust_xe = xeflat_left - traiectorie_robot_robust(2, :);
flat_right_robust_ye = yeflat_right - traiectorie_robot_robust(3, :);
flat_right_robust_phie = phieflat_right - traiectorie_robot_robust(4, :);

% antifragile control analysis
bump_left_antifragile_xe = xebump_left - traiectorie_robot_antifragile(2, :);
bump_left_antifragile_ye = yebump_left - traiectorie_robot_antifragile(3, :);
bump_left_antifragile_phie = phiebump_left - traiectorie_robot_antifragile(4, :);

bump_right_antifragile_xe = xebump_right - traiectorie_robot_antifragile(2, :);
bump_right_antifragile_ye = yebump_right - traiectorie_robot_antifragile(3, :);
bump_right_antifragile_phie = phiebump_right - traiectorie_robot_antifragile(4, :);

flat_left_antifragile_xe = xeflat_left - traiectorie_robot_antifragile(2, :);
flat_left_antifragile_ye = yeflat_left - traiectorie_robot_antifragile(3, :);
flat_left_antifragile_phie = phieflat_left - traiectorie_robot_antifragile(4, :);

flat_right_antifragile_xe = xeflat_left - traiectorie_robot_antifragile(2, :);
flat_right_antifragile_ye = yeflat_right - traiectorie_robot_antifragile(3, :);
flat_right_antifragile_phie = phieflat_right - traiectorie_robot_antifragile(4, :);

% resilient control analysis
bump_left_resilient_xe = xebump_left - traiectorie_robot_resilient(1, :);
bump_left_resilient_ye = yebump_left - traiectorie_robot_resilient(2, :);
bump_left_resilient_phie = phiebump_left - traiectorie_robot_resilient(3, :);

bump_right_resilient_xe = xebump_right - traiectorie_robot_resilient(1, :);
bump_right_resilient_ye = yebump_right - traiectorie_robot_resilient(2, :);
bump_right_resilient_phie = phiebump_right - traiectorie_robot_resilient(3, :);

flat_left_resilient_xe = xeflat_left - traiectorie_robot_resilient(1, :);
flat_left_resilient_ye = yeflat_left - traiectorie_robot_resilient(2, :);
flat_left_resilient_phie = phieflat_left - traiectorie_robot_resilient(3, :);

flat_right_resilient_xe = xeflat_left - traiectorie_robot_resilient(1, :);
flat_right_resilient_ye = yeflat_right - traiectorie_robot_resilient(2, :);
flat_right_resilient_phie = phieflat_right - traiectorie_robot_resilient(3, :);


% fault free vals
xeRMSErobustFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_robust(2, :)).^2));
xeRMSEadaptiveFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_adaptive(2, :)).^2));
xeRMSEresilientFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_resilient(1, :)).^2));
xeRMSEantifragileFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_antifragile(2, :)).^2));

yeRMSErobustFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_robust(3, :)).^2));
yeRMSEadaptiveFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_adaptive(3, :)).^2));
yeRMSEresilientFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_resilient(2, :)).^2));
yeRMSEantifragileFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_antifragile(3, :)).^2));

phieRMSErobustFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_robust(4, :)).^2));
phieRMSEadaptiveFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_adaptive(4, :)).^2));
phieRMSEresilientFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_resilient(3, :)).^2));
phieRMSEantifragileFaultFree = sqrt(mean((traiectorie_ideala - traiectorie_robot_antifragile(4, :)).^2));

% bump left errors

xeRMSErobustBumpLeft = sqrt(mean(bump_left_robust_xe).^2);
xeRMSEadaptiveBumpLeft = sqrt(mean(bump_left_adaptive_xe).^2);
xeRMSEresilientBumpLeft = sqrt(mean(bump_left_resilient_xe).^2);
xeRMSEantifragileBumpLeft = sqrt(mean(bump_left_antifragile_xe).^2);

yeRMSErobustBumpLeft = sqrt(mean(bump_left_robust_ye).^2);
yeRMSEadaptiveBumpLeft = sqrt(mean(bump_left_adaptive_ye).^2);
yeRMSEresilientBumpLeft = sqrt(mean(bump_left_resilient_ye).^2);
yeRMSEantifragileBumpLeft = sqrt(mean(bump_left_antifragile_ye).^2);

phieRMSErobustBumpLeft = sqrt(mean(bump_left_robust_phie).^2);
phieRMSEadaptiveBumpLeft = sqrt(mean(bump_left_adaptive_phie).^2);
phieRMSEresilientBumpLeft = sqrt(mean(bump_left_resilient_phie).^2);
phieRMSEantifragileBumpLeft = sqrt(mean(bump_left_antifragile_phie).^2);

% bump right errors

xeRMSErobustBumpRight= sqrt(mean(bump_right_robust_xe).^2);
xeRMSEadaptiveBumpRight = sqrt(mean(bump_right_adaptive_xe).^2);
xeRMSEresilientBumpRight = sqrt(mean(bump_right_resilient_xe).^2);
xeRMSEantifragileBumpRight = sqrt(mean(bump_right_antifragile_xe).^2);

yeRMSErobustBumpRight = sqrt(mean(bump_right_robust_ye).^2);
yeRMSEadaptiveBumpRight = sqrt(mean(bump_right_adaptive_ye).^2);
yeRMSEresilientBumpRight = sqrt(mean(bump_right_resilient_ye).^2);
yeRMSEantifragileBumpRight = sqrt(mean(bump_right_antifragile_ye).^2);

phieRMSErobustBumpRight = sqrt(mean(bump_right_robust_phie).^2);
phieRMSEadaptiveBumpRight = sqrt(mean(bump_right_adaptive_phie).^2);
phieRMSEresilientBumpRight = sqrt(mean(bump_right_resilient_phie).^2);
phieRMSEantifragileBumpRight = sqrt(mean(bump_right_antifragile_phie).^2);


% flat tire left errors
xeRMSErobustFlatLeft= sqrt(mean(flat_left_robust_xe).^2);
xeRMSEadaptiveFlatLeft = sqrt(mean(flat_left_adaptive_xe).^2);
xeRMSEresilientFlatLeft = sqrt(mean(flat_left_resilient_xe).^2);
xeRMSEantifragileFlatLeft = sqrt(mean(flat_left_antifragile_xe).^2);

yeRMSErobustFlatLeft = sqrt(mean(flat_left_robust_ye).^2);
yeRMSEadaptiveFlatLeft = sqrt(mean(flat_left_adaptive_ye).^2);
yeRMSEresilientFlatLeft = sqrt(mean(flat_left_resilient_ye).^2);
yeRMSEantifragileFlatLeft = sqrt(mean(flat_left_antifragile_ye).^2);

phieRMSErobustFlatLeft = sqrt(mean(flat_left_robust_phie).^2);
phieRMSEadaptiveFlatLeft = sqrt(mean(flat_left_adaptive_phie).^2);
phieRMSEresilientFlatLeft = sqrt(mean(flat_left_resilient_phie).^2);
phieRMSEantifragileFlatLeft = sqrt(mean(flat_left_antifragile_phie).^2);


% flat tire right errors
xeRMSErobustFlatRight= sqrt(mean(flat_right_robust_xe).^2);
xeRMSEadaptiveFlatRight = sqrt(mean(flat_right_adaptive_xe).^2);
xeRMSEresilientFlatRight = sqrt(mean(flat_right_resilient_xe).^2);
xeRMSEantifragileFlatRight = sqrt(mean(flat_right_antifragile_xe).^2);

yeRMSErobustFlatRight = sqrt(mean(flat_right_robust_ye).^2);
yeRMSEadaptiveFlatRight = sqrt(mean(flat_right_adaptive_ye).^2);
yeRMSEresilientFlatRight = sqrt(mean(flat_right_resilient_ye).^2);
yeRMSEantifragileFlatRight = sqrt(mean(flat_right_antifragile_ye).^2);

phieRMSErobustFlatRight = sqrt(mean(flat_right_robust_phie).^2);
phieRMSEadaptiveFlatRight = sqrt(mean(flat_right_adaptive_phie).^2);
phieRMSEresilientFlatRight = sqrt(mean(flat_right_resilient_phie).^2);
phieRMSEantifragileFlatRight = sqrt(mean(flat_right_antifragile_phie).^2);

% fault free error
faultfree_adaptive_xe = traiectorie_ideala(2, :) - traiectorie_robot_adaptive(2, :);
faultfree_adaptive_ye = traiectorie_ideala(3, :) - traiectorie_robot_adaptive(3, :);
faultfree_adaptive_phie = traiectorie_ideala(4, :) - traiectorie_robot_adaptive(4, :);

faultfree_robust_xe = traiectorie_ideala(2, :) - traiectorie_robot_robust(2, :);
faultfree_robust_ye = traiectorie_ideala(3, :) - traiectorie_robot_robust(3, :);
faultfree_robust_phie = traiectorie_ideala(4, :) - traiectorie_robot_robust(4, :);

faultfree_antifragile_xe = traiectorie_ideala(2, :) - traiectorie_robot_antifragile(2, :);
faultfree_antifragile_ye = traiectorie_ideala(3, :)- traiectorie_robot_antifragile(3, :);
faultfree_antifragile_phie = traiectorie_ideala(4, :) - traiectorie_robot_antifragile(4, :);

faultfree_resilient_xe = traiectorie_ideala(2, :) - traiectorie_robot_resilient(1, :);
faultfree_resilient_ye =traiectorie_ideala(3, :) - traiectorie_robot_resilient(2, :);
faultfree_resilient_phie = traiectorie_ideala(4, :) - traiectorie_robot_resilient(3, :);


xeRMSErobustFaultfree= sqrt(mean(faultfree_robust_xe).^2);
xeRMSEadaptiveFaultfree = sqrt(mean(faultfree_adaptive_xe).^2);
xeRMSEresilientFaultfree= sqrt(mean(faultfree_resilient_xe).^2);
xeRMSEantifragileFaultfree = sqrt(mean(faultfree_antifragile_xe).^2);

yeRMSErobustFaultfree = sqrt(mean(faultfree_robust_ye).^2);
yeRMSEadaptiveFaultfree = sqrt(mean(faultfree_adaptive_ye).^2);
yeRMSEresilientFaultfree = sqrt(mean(faultfree_resilient_ye).^2);
yeRMSEantifragileFaultfree = sqrt(mean(faultfree_antifragile_ye).^2);

phieRMSErobustFaultfree = sqrt(mean(faultfree_robust_phie).^2);
phieRMSEadaptiveFaultfree = sqrt(mean(faultfree_adaptive_phie).^2);
phieRMSEresilientFaultfree = sqrt(mean(faultfree_resilient_phie).^2);
phieRMSEantifragileFaultfree = sqrt(mean(faultfree_antifragile_phie).^2);